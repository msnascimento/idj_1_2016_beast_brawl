#ifndef UI_H
#define UI_H

#include <unordered_map>
#include <memory>
#include "Rect.h"
#include "Vec2.h"
#include "Sprite.h"

class UI
{
    public:
        enum class Snap{LEFT, RIGHT, UP, DOWN, CENTER, CENTERLEFT, CENTERRIGHT, CENTERUP, CENTERDOWN, TOPLEFT, TOPRIGHT, BOTTOMLEFT, BOTTOMRIGHT, UNDEFINED};
        //Construtor sem inicialização
        UI();
        //Construtor com inicialização e snap vertical E horizontal
        //Passar nullptr para outer significa que o elemento não é composto
        UI(int id, float  w, float h, Snap snap, UI *outer);
        UI(int id, UI* outer);   //Construtor sem largura e altura definida
        //Construtor com inicialização e snap vertical OU horizontal ("pos" pode definir x OU y, dependendo do Snap passado)
        UI(int id, float w, float h, float  pos, Snap snap, UI *outer);
        virtual ~UI();
        virtual void Update(float dt) = 0;
        virtual void UpdateInner(float dt);
        virtual void Render() = 0;
        virtual void RenderInner();

        //Atualiza o snap, atualizando a posição do rect
        //Chamar quando usar construtor default ou quando houver mudança no tamanho da janela
        //Nao chamar sem inicializar o w e h de box e outer
        void SetSnap(float pos, Snap snap);
        void SetSnap(Snap snap);
        //Para elementos compostos, passe um ponteiro para o elementos principal, para o calculo da posição relativa
        void SetOuterUI(UI* outer);
        UI* GetOuterUI();
        void ClearOuter();
        void AddInnerUI(UI* inner);
        void RemoveInner(int id);
        int id;
        Rect box;
    protected:
        Sprite sp;
        std::unordered_map<int, std::shared_ptr<UI>> innerArray;
    private:
        Snap snap;
        UI *outer;

};

#endif // UI_H
