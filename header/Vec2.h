#ifndef HEADER_VEC
#define HEADER_VEC

#include <cmath>

class Vec2{



public:
float x;
float y;

    Vec2(){}
    Vec2(float x, float y){

        this->x=x;
        this->y=y;

    }
    Vec2(Vec2 pos, float mod, float angle)
    {
        this->x = mod;
        this->y = 0;
        Rot(angle);
        (*this)+=pos;
    }

    void Rot(float angle)
    {
        float temp = this->x;

        this->x = this->x * cos(angle) - this->y * sin(angle);
        this->y = this->y * cos(angle) + temp * sin(angle);
    }

    float distancia(Vec2 pos){

            return sqrt(   (this->x-pos.x)*(this->x-pos.x) +
            (this->y-pos.y)*(this->y-pos.y));
    }

    void operator+=(const Vec2& op)
    {
        this->x+=op.x;
        this->y+=op.y;
    }

    Vec2 operator+(const Vec2& rhs) const {
    return Vec2(x + rhs.x, y + rhs.y);
 }

 Vec2 operator-(const Vec2& rhs) const {
    return Vec2(x - rhs.x, y - rhs.y);
 }

 Vec2 operator*(const float rhs) const {
    return Vec2(x * rhs, y * rhs);
}


};
#endif
