

#include "../header/SelectState.h"
#include "../header/Game.h"
#include "../header/InputManager.h"
#include "../header/Text.h"
#include "../header/FightState.h"
#include "../header/Resources.h"
#include "../header/Sound.h"

//#define FONT "font/Call me maybe.ttf"
#define FONT "font/werebeast.ttf"

#define persongaem1_y 140
#define persongaem2_y  persongaem1_y
#define persongaem3_y  persongaem1_y
#define persongaem4_y  persongaem1_y

#define persongaem1_x 220
#define persongaem2_x 500
#define persongaem3_x 780
#define persongaem4_x 1060




SelectState::SelectState(){
        //instaciar Sprite bg
        this->bg=*(new Sprite("img/hud/selecao de personagens.png"));
        bg.SetClip(0,0,Game::GetInstance().GetDisplay().x,Game::GetInstance().GetDisplay().y);


      //Tratamento da imagem do personagem
        this->ps1_sp=*(new Sprite("img/hud/animais.png",4,99999));
        this->ps2_sp=*(new Sprite("img/hud/animais.png",4,99999));
        this->ps3_sp=*(new Sprite("img/hud/animais.png",4,99999));
        this->ps4_sp=*(new Sprite("img/hud/animais.png",4,99999));


        ps1_off_sp.Open("img/hud/tubarao_off.png");
        ps2_off_sp.Open("img/hud/tigre_off.png");
        ps3_off_sp.Open("img/hud/rhino_off.png");
        ps4_off_sp.Open("img/hud/urso_off.png");
        player_off_sp.Open("img/hud/player_off.png");

        ps1_sp.SetClip(0, 0, ps1_sp.GetWidth(), ps1_sp.GetHeight());
        ps2_sp.SetClip(0, 0, ps2_sp.GetWidth(), ps2_sp.GetHeight());
        ps3_sp.SetClip(0, 0, ps3_sp.GetWidth(), ps3_sp.GetHeight());
        ps4_sp.SetClip(0, 0, ps4_sp.GetWidth(), ps4_sp.GetHeight());

        ps1_off_sp.SetClip(0, 0, ps1_off_sp.GetWidth(), ps1_off_sp.GetHeight());
        ps2_off_sp.SetClip(0, 0, ps2_off_sp.GetWidth(), ps2_off_sp.GetHeight());
        ps3_off_sp.SetClip(0, 0, ps3_off_sp.GetWidth(), ps3_off_sp.GetHeight());
        ps4_off_sp.SetClip(0, 0, ps4_off_sp.GetWidth(), ps4_off_sp.GetHeight());


        player_off_sp.SetClip(0, 0, player_off_sp.GetWidth(), player_off_sp.GetHeight());

        for(int i=0;i<4;i++){
            posicao[i]=i;
            posicao_off[i]=-1;
        }

        //letras
        SDL_Color cor;
        cor.a=255;
        cor.r=200;cor.g=0;cor.b=0;

        cor.r=255;cor.g=0;cor.b=0;
        selecione=new Text(FONT,50,SOLID,"Selecione seu personagem!",cor);
        start=new Text(FONT,50,SOLID,"Fight!",cor);

        selecione->SetPos(Game::GetInstance().GetDisplay().x/2,Game::GetInstance().GetDisplay().y/4.5);
        start->SetPos(Game::GetInstance().GetDisplay().x/2,Game::GetInstance().GetDisplay().y/10);

        //musica
        music= new Music();
        music->Open("audio/Musicas/Possible_BGM2.wav");
        opcao= new Sound();
        opcao->Open("audio/Sons/WAV/Select1.wav");

        contador1=0;
        contador2=240;
        contador3=480;
        contador4=720;
}

SelectState::~SelectState(){
        objectArray.clear();
}


void SelectState::UpdateArray(float dt){

    //object array
    for(int i=0;i<objectArray.size();i++){
        objectArray[i].get()->Update(dt);
        if(objectArray[i].get()->IsDead()){
                objectArray.erase(objectArray.begin()+i);
        }
    }
}

void SelectState::Update(){

        //quit
        if(InputManager::GetInstance().KeyPress(SDLK_ESCAPE)){
           this->quitRequested=true;
            this->popRequested=true;}
        if(InputManager::GetInstance().QuitRequested())
           this->quitRequested=true;


        SDL_Color cor;
        cor.a=255;cor.r=150;cor.g=150;cor.b=150;
        SDL_Color cor2;
        cor2.a=255;cor2.r=0;cor2.g=0;cor2.b=0;
        //texto
        if(start->IsMouse_TextInside()){
                start->SetColor(cor);
                if(InputManager::GetInstance().IsMouseDown(LEFT_MOUSE_BUTTON)){
                    std::string players[4];
                    for(int i=0;i<4;i++){
                        switch(posicao[i]){
                            case 0: players[i]="shark_black";break;
                            case 1: players[i]="tigreza";break;
                            case 2: players[i]="rhino";break;
                            case 3: players[i]="urso";break;
                            default: players[i]="shark_black";break;
                        }
                    }
                    music->Stop();
                    opcao->Play(1);
                    popRequested=true;
                    Game::GetInstance().Push(new FightState(players));
                }
        }else
            start->SetColor(cor2);

         //personagem
        for(int i=0;i<InputManager::GetInstance().GetJoy();i++){
            if(InputManager::GetInstance().JoyPress(i,JOY_LEFT)){
                posicao[i]-=1;
                if(posicao[i]<0)
                    posicao[i]=0;
            }
            if(InputManager::GetInstance().JoyPress(i,JOY_RIGHT)){
                posicao[i]+=1;
                if(posicao[i]>3)
                    posicao[i]=3;
            }
            if(InputManager::GetInstance().JoyPress(i,JOY_X)){
                if(posicao_off[i]==-1)
                    posicao_off[i]=posicao[i];
            }


        }

        if(posicao_off[0]==-1){
            if(posicao[0]==0 && contador1>0){
                contador1-=10;
            }else if(posicao[0]==1){
                if(contador1>245)
                    contador1-=10;
                else if(contador1<235)
                    contador1+=10;
            } else if(posicao[0]==2){
                if(contador1>485)
                    contador1-=10;
                else if(contador1<475)
                    contador1+=10;
            }else if(posicao[0]==3 && contador1<715){
                    contador1+=10;
            }
        }

        if(posicao_off[1]==-1){
            if(posicao[1]==0 && contador2>0){
                contador2-=10;
            }else if(posicao[1]==1){
                if(contador2>245)
                    contador2-=10;
                else if(contador2<235)
                    contador2+=10;
            } else if(posicao[1]==2){
                if(contador2>485)
                    contador2-=10;
                else if(contador2<475)
                    contador2+=10;
            }else if(posicao[1]==3 && contador2<715){
                contador2+=10;
            }
        }

        if(posicao_off[2]==-1){
            if(posicao[2]==0 && contador3>0){
                contador3-=10;
            }else if(posicao[2]==1){
                if(contador3>245)
                    contador3-=10;
                else if(contador3<235)
                    contador3+=10;
            } else if(posicao[2]==2){
                if(contador3>485)
                    contador2-=10;
                else if(contador3<475)
                    contador3+=10;
            }else if(posicao[2]==3 && contador3<715){
                contador3+=10;
            }
        }

        if(posicao_off[3]==-1){
             if(posicao[3]==0 && contador4>0){
                contador4-=10;
            }else if(posicao[3]==1){
                if(contador4>245)
                    contador4-=10;
                else if(contador4<235)
                    contador4+=10;
            } else if(posicao[3]==2){
                if(contador4>485)
                    contador4-=10;
                else if(contador4<475)
                    contador4+=10;
            }else if(posicao[3]==3 && contador4<715){
                contador4+=10;
            }
        }
    ps1_sp.SetClip(contador1,0,240,ps1_sp.GetHeight());
    ps2_sp.SetClip(contador2,0,240,ps2_sp.GetHeight());
    ps3_sp.SetClip(contador3,0,240,ps3_sp.GetHeight());
    ps4_sp.SetClip(contador4,0,240,ps4_sp.GetHeight());
}

void SelectState::Render(){


        bg.Render(0,0);
        //pesronagens

            player_off_sp.Render(persongaem4_x-ps4_sp.GetWidth()*0.5,persongaem4_y);
            player_off_sp.Render(persongaem3_x-ps3_sp.GetWidth()*0.5,persongaem3_y);
            player_off_sp.Render(persongaem2_x-ps2_sp.GetWidth()*0.5,persongaem2_y);
            player_off_sp.Render(persongaem1_x-ps1_sp.GetWidth()*0.5,persongaem1_y);

         switch(InputManager::GetInstance().GetJoy()){

            case 4:  ps4_sp.Render(persongaem4_x-ps4_sp.GetWidth()*0.5,persongaem4_y);
            case 3:  ps3_sp.Render(persongaem3_x-ps3_sp.GetWidth()*0.5,persongaem3_y);
            case 2:  ps2_sp.Render(persongaem2_x-ps2_sp.GetWidth()*0.5,persongaem2_y);
            case 1:  ps1_sp.Render(persongaem1_x-ps1_sp.GetWidth()*0.5,persongaem1_y);
        }

         switch(InputManager::GetInstance().GetJoy()){

            case 4:
                switch(posicao_off[3]){
                    case -1: break;
                    case  0: ps1_off_sp.Render(persongaem4_x-ps4_sp.GetWidth()*0.5,persongaem4_y); break;
                    case  1: ps2_off_sp.Render(persongaem4_x-ps4_sp.GetWidth()*0.5,persongaem4_y); break;
                    case  2: ps3_off_sp.Render(persongaem4_x-ps4_sp.GetWidth()*0.5,persongaem4_y); break;
                    case  3: ps4_off_sp.Render(persongaem4_x-ps4_sp.GetWidth()*0.5,persongaem4_y); break;
                }
            case 3:
                switch(posicao_off[2]){
                    case -1: break;
                    case  0: ps1_off_sp.Render(persongaem3_x-ps3_sp.GetWidth()*0.5,persongaem3_y); break;
                    case  1: ps2_off_sp.Render(persongaem3_x-ps3_sp.GetWidth()*0.5,persongaem3_y); break;
                    case  2: ps3_off_sp.Render(persongaem3_x-ps3_sp.GetWidth()*0.5,persongaem3_y); break;
                    case  3: ps4_off_sp.Render(persongaem3_x-ps3_sp.GetWidth()*0.5,persongaem3_y); break;
                }
            case 2:
                switch(posicao_off[1]){
                    case -1: break;
                    case  0: ps1_off_sp.Render(persongaem2_x-ps2_sp.GetWidth()*0.5,persongaem2_y); break;
                    case  1: ps2_off_sp.Render(persongaem2_x-ps2_sp.GetWidth()*0.5,persongaem2_y); break;
                    case  2: ps3_off_sp.Render(persongaem2_x-ps2_sp.GetWidth()*0.5,persongaem2_y); break;
                    case  3: ps4_off_sp.Render(persongaem2_x-ps2_sp.GetWidth()*0.5,persongaem2_y); break;
                }
            case 1:
                switch(posicao_off[0]){
                    case -1: break;
                    case  0: ps1_off_sp.Render(persongaem1_x-ps1_sp.GetWidth()*0.5,persongaem1_y); break;
                    case  1: ps2_off_sp.Render(persongaem1_x-ps1_sp.GetWidth()*0.5,persongaem1_y); break;
                    case  2: ps3_off_sp.Render(persongaem1_x-ps1_sp.GetWidth()*0.5,persongaem1_y); break;
                    case  3: ps4_off_sp.Render(persongaem1_x-ps1_sp.GetWidth()*0.5,persongaem1_y); break;
                }
        }

        //selecione->Render();

        //se todos ja tiverem selecionados renderiza o start
        //start->Reender();
    }



void SelectState::Resume(){}

void SelectState::Pause(){}


