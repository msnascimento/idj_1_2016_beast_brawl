#ifndef BINDSKILL_H
#define BINDSKILL_H

#include "../header/Skill.h"
#include "../header/Projectile.h"

#define BIND_BASE_DMG 0
#define BIND_SPEEDMOD 50
#define BIND_BASE_CD 1
#define BIND_SPRITE "img/skills/pokeball.png"
#define BIND_CONTACT_SPRITE "img/efeitos/effect1.png"
#define BIND_CONTACT_SOUND "audio/Sons/WAV/Hit2.wav"
#define BIND_DURATION 2

//Skill que prende
//Player atira um projetil(mesma logica de projectile) que prende quando em contato com inimigo
//A logica da skill esta basicamente no IsColliding do player
class BindSkill : public Projectile
{
    public:
        BindSkill(Vec2 orig, float angle, int source);
        BindSkill();
        virtual ~BindSkill();
        void NotifyCollision (GameObject& other);
        bool Is (string type);
        Skill* NewInstance(Vec2 orig, float angle, int source);
    protected:
    private:
};

#endif // BINDSKILL_H
