#ifndef HEADER_MUSIC
#define HEADER_MUSIC
#include <SDL2/SDL_mixer.h>
#include <string>
class Music{


public:
    Music();
    Music(std::string file);

    void Play (int times);
    void Stop ();
    void Open (std::string file);
    bool IsOpen ();

 private:
    Mix_Music* music;

};
#endif

