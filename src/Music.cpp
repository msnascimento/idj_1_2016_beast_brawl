

#include "../header/Music.h"
#include "../header/Resources.h"


    Music::Music(){
        music=nullptr;
    }

    Music::Music(std::string file){
        Open(file);
    }

    void Music::Play (int times){
    if (music!=nullptr)
        Mix_PlayMusic(music,times);
    }

    void Music::Stop (){
        Mix_FadeOutMusic(0);
    }

    void Music::Open (std::string file){
        music=Resources::GetMusic(file);
    }

    bool Music::IsOpen (){

        if (music==nullptr){
            return false;
        }
        return true;
    }


