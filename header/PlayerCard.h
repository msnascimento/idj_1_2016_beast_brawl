#ifndef PLAYERCARD_H
#define PLAYERCARD_H

#include "../header/UI.h"
#include "../header/Text.h"

#define CARD_HPIMG "img/hud/hpbar.png"
#define BLUEORB "img/hud/orbs/blue.png"
#define YELLOWORB "img/hud/orbs/yellow.png"
#define GREENORB "img/hud/orbs/green.png"

using namespace std;

class PlayerCard : public UI
{
    public:
        PlayerCard(int id, Snap snap, string img, UI* outer);
        virtual ~PlayerCard();
        void Update(float dt);
        void Render();
        void SetHP(float value);    //Seta um valor pra barra de hp em porcentagem
        void SetElements(int elements[3]);
    protected:
    private:
        Sprite hpsp;
        Rect hpbox;

        Sprite orbs_sprite[3];
        Rect orbsbox[3];
};

#endif // PLAYERCARD_H
