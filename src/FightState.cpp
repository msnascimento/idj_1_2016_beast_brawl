#include "../header/FightState.h"
#include "../header/Game.h"
#include "../header/InputManager.h"
#include "../header/Camera.h"
#include "../header/Collision.h"
#include "../header/Text.h"
#include "../header/StateData.h"
#include "../header/EndState.h"
#include "../header/Resources.h"
#include "../header/Player.h"
#include "../header/Objetos.h"
#include "../header/EndState.h"

#include <ctime>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <memory>
#include <time.h>


#define escala 2
#define tile_side 20
enum Map{
layer=360,
under=20,
domino=96,
carta=144,
peca1=145,
peca2=192,
peca3=194
};



FightState::FightState(std::string personagens[]){
        //instaciar Sprite bg
//       FightState::bg=*(new Sprite("img/background2.png"));
        bg.SetClip(0,0,Game::GetInstance().GetDisplay().x,Game::GetInstance().GetDisplay().y);
        tileSet= new TileSet(tile_side,tile_side,"img/cenario/tileset.png",escala);
        tileMap= new TileMap("map/tilemap.txt",tileSet);
        srand(time(NULL));

        //gerar peças aleatórias
        //dominó 96~119/120~143  sombra 0
        //carta 144/168
        //peça lego 1 145-146/169-170   sobra 3-4
        //peça lego 2 192-193/216-217   sombra 2
        //peça lego 3 194-195/218-219   sombra 3-4
        tileMap->SetMap(Gerar_Dominos(tileMap->GetMap()));
        tileMap->SetMap((Gerar_Legos(tileMap->GetMap())));
        tileMap->SetMap((Gerar_Cartas(tileMap->GetMap())));

        Layer2Obj(2,tileMap->GetMap());


        //numero de players= numero de controles4);
        switch(InputManager::GetInstance().GetJoy()){
            Player* p1;
            case 4:
                hud.AddCard(3, UI::Snap::BOTTOMRIGHT, "img/hud/vida_"+personagens[3 ]+".png");
                p1= new Player(980,405, personagens[3],3);
                AddObject(p1);

            case 3:
                hud.AddCard(2, UI::Snap::BOTTOMLEFT, "img/hud/vida_"+personagens[2]+".png");
                p1= new Player(300,405, personagens[2],2);
                AddObject(p1);

            case 2:
                hud.AddCard(1, UI::Snap::TOPRIGHT, "img/hud/vida_"+personagens[1]+".png");
                p1= new Player(975,85, personagens[1],1);
                AddObject(p1);

           case 1:
                hud.AddCard(0, UI::Snap::TOPLEFT, "img/hud/vida_"+personagens[0]+".png");
                p1= new Player(305,80, personagens[0],0);
                AddObject(p1);
            default: break;
        }

        //musica
        music = new Music();
        music->Open("audio/Musicas/Possible_BGM1.wav");
        music->Play(-1);

        if(InputManager::GetInstance().GetJoy()<2){// teste sem joypad
            Player*  p1= new Player(975,85, personagens[2],1);
            AddObject(p1);
            hud.AddCard(1, UI::Snap::TOPRIGHT, "img/hud/vida_"+personagens[2]+".png");
        }
        if(InputManager::GetInstance().GetJoy()<1){// teste sem joypad
            Player*  p1= new Player(305,85, personagens[3],0);
             AddObject(p1);
            hud.AddCard(0, UI::Snap::TOPLEFT, "img/hud/vida_"+personagens[1]+".png");
        }

}

FightState::~FightState(){
        objectArray.clear();
        objectArray_low.clear();
        objectArray_high.clear();
}


void FightState::UpdateArray(float dt){

    //object array
    int players=0;
    Player* p;
    for(int i=0;i<objectArray.size();i++){
        objectArray[i].get()->Update(dt);
        if(objectArray[i].get()->IsDead()){
                objectArray.erase(objectArray.begin()+i);
        }
    }

    for(int i=0;i<objectArray_low.size();i++){
        objectArray_low[i].get()->Update(dt);
        if(objectArray_low[i].get()->IsDead()){
                objectArray_low.erase(objectArray_low.begin()+i);
        }
    }

    for(int i=0;i<objectArray_high.size();i++){
        objectArray_high[i].get()->Update(dt);
        if(objectArray_high[i].get()->IsDead()){
                objectArray_high.erase(objectArray_high.begin()+i);
        }
    }

     for(int i=0;i<objectArray.size();i++){
            if(objectArray[i].get()->Is("Player")){
                players+=1;
                p= (Player*)(objectArray[i].get());
            }
        }

    if(players<2){
        if(p!=nullptr){

            StateData statedata;

            statedata.nPlayer=p->nPlayer;
            statedata.name=p->GetName();
            popRequested=true;
            Game::GetInstance().Push(new EndState(statedata));
            music->Stop();
            music->Open("audio/Musicas/Possible_BGM1.wav");
            music->Play(-1);
        }
    }
}



void FightState::Update(){

        Camera::Update(Game::GetInstance().GetDeltaTime());

        //quit
        if(InputManager::GetInstance().KeyPress(SDLK_ESCAPE)){
            this->quitRequested=true;
            this->popRequested=true;
            music->Stop();
            music->Open("audio/Musicas/Possible_BGM2.wav");
            music->Play(-1);
        }
        if(InputManager::GetInstance().QuitRequested())
           this->quitRequested=true;


        //colisao de player e skills com -> objtos
        if(objectArray.size()>=1){
            for(int i=0;i<objectArray.size()-1;i++){
                if(objectArray[i].get()->Is("Objetos")){
                    for(int j=i+1;j<objectArray.size();j++){

                        if( Collision::IsColliding(objectArray[i].get()->box,objectArray[j].get()->box,objectArray[i].get()->rotation,objectArray[j].get()->rotation)){
                            if(!objectArray[j].get()->Is("Objetos")){
                                objectArray[i].get()->NotifyCollision(*objectArray[j].get());
                                objectArray[j].get()->NotifyCollision(*objectArray[i].get());
                            }
                        }
                    }
                }
            }

        }

        //colisão de players <-> skill
          if(objectArray.size()>=1){
            for(int i=0;i<objectArray.size()-1;i++){
                if(objectArray[i].get()->Is("Player") || objectArray[i].get()->Is("Shield")){
                    for(int j=i+1;j<objectArray.size();j++){
                        if( Collision::IsColliding(objectArray[i].get()->box_projectile_colision,objectArray[j].get()->box_projectile_colision,objectArray[i].get()->rotation,objectArray[j].get()->rotation)){
                            if(objectArray[j].get()->Is("Skill") || objectArray[j].get()->Is("Projectile")){
                                objectArray[i].get()->NotifyCollision(*objectArray[j].get());
                                objectArray[j].get()->NotifyCollision(*objectArray[i].get());
                            }
                        }
                    }
                }
            }

        }

            for(int i=0;i<objectArray_low.size();i++){
                if(objectArray_low[i].get()->Is("Banana")){
                    for(int j=0;j<objectArray.size();j++){
                        if( Collision::IsColliding(objectArray_low[i].get()->box,objectArray[j].get()->box,objectArray_low[i].get()->rotation,objectArray[j].get()->rotation)){
                            if(objectArray[j].get()->Is("Player")){
                                if( Collision::IsColliding(objectArray_low[i].get()->box_projectile_colision,objectArray[j].get()->box,objectArray_low[i].get()->rotation,objectArray[j].get()->rotation)){
                                    if(objectArray[j].get()->Is("Player")){
                                        objectArray_low[i].get()->NotifyCollision(*objectArray[j].get());
                                        objectArray[j].get()->NotifyCollision(*objectArray_low[i].get());
                                    }
                                }
                            }
                        }
                    }
                }
            }

         //update
         UpdateArray(Game::GetInstance().GetDeltaTime());

}




void FightState::RenderArray(){



    for(int i=0;i<objectArray.size();i++){

        State::objectArray[i].get()->Render();
            //quadrado em volta


     /*      Vec2 v;
            int x1,x2,y1,y2;

            v=Vec2(objectArray[i].get()->box.getX()-Camera::pos.x,objectArray[i].get()->box.getY()-Camera::pos.y);
             x1=v.x-objectArray[i].get()->box.getW()/2;
             y1=v.y-objectArray[i].get()->box.getH()/2;
             x2=v.x-objectArray[i].get()->box.getW()/2;
             y2=v.y+objectArray[i].get()->box.getH()/2;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x+objectArray[i].get()->box.getW()/2;
            y1=v.y-objectArray[i].get()->box.getH()/2;
            x2=v.x+objectArray[i].get()->box.getW()/2;
            y2=v.y+objectArray[i].get()->box.getH()/2;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x-objectArray[i].get()->box.getW()/2;
            y1=v.y+objectArray[i].get()->box.getH()/2;
            x2=v.x+objectArray[i].get()->box.getW()/2;
            y2=v.y+objectArray[i].get()->box.getH()/2;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x-objectArray[i].get()->box.getW()/2;
            y1=v.y-objectArray[i].get()->box.getH()/2;
            x2=v.x+objectArray[i].get()->box.getW()/2;
            y2=v.y-objectArray[i].get()->box.getH()/2;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);*/


         /*   v=Vec2(objectArray[i].get()->box_projectile_colision.getX()-Camera::pos.x,projectileArray[i].get()->box_projectile_colision.getY()-Camera::pos.y);
            x1=v.x-objectArray[i].get()->box_projectile_colision.getW()/2;
            y1=v.y-objectArray[i].get()->box_projectile_colision.getH()/2;
            x2=v.x-objectArray[i].get()->box_projectile_colision.getW()/2;
            y2=v.y+objectArray[i].get()->box_projectile_colision.getH()/2;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x+objectArray[i].get()->box_projectile_colision.getW()/2;
            y1=v.y-objectArray[i].get()->box_projectile_colision.getH()/2;
            x2=v.x+objectArray[i].get()->box_projectile_colision.getW()/2;
            y2=v.y+objectArray[i].get()->box_projectile_colision.getH()/2;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x-objectArray[i].get()->box_projectile_colision.getW()/2;
            y1=v.y+objectArray[i].get()->box_projectile_colision.getH()/2;
            x2=v.x+objectArray[i].get()->box_projectile_colision.getW()/2;
            y2=v.y+objectArray[i].get()->box_projectile_colision.getH()/2;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x-objectArray[i].get()->box_projectile_colision.getW()/2;
            y1=v.y-objectArray[i].get()->box_projectile_colision.getH()/2;
            x2=v.x+objectArray[i].get()->box_projectile_colision.getW()/2;
            y2=v.y-objectArray[i].get()->box_projectile_colision.getH()/2;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);*/
        }

}



void FightState::Render(){

    int x=0,y=0;

        bg.Render(x,y);

    //Camera::speed.x=500;
    //Camera::speed.y=500;

    //0-chao
    //1-sombra
    //2-paredes objetos baixo
    //3 skill low
    //4 personagens skill
    //5 skill high
    //6 objetos cima
    //7-paredes baixo
        tileMap->RenderLayer(0,120-Camera::pos.x*1.0, -Camera::pos.y*1.0);
        tileMap->RenderLayer(1,120-Camera::pos.x*1.05, -Camera::pos.y*1.05);
        tileMap->RenderLayer(2,120-Camera::pos.x*1.1, -Camera::pos.y*1.1);


        for(int i=0;i<objectArray_low.size();i++){
            this->objectArray_low[i].get()->Render();
        }
        for(int i=0;i<objectArray.size();i++){
            this->objectArray[i].get()->Render();


          /*      Vec2 v;
            int x1,x2,y1,y2;

          v=Vec2(objectArray[i].get()->box.getX()-Camera::pos.x,objectArray[i].get()->box.getY()-Camera::pos.y);
             x1=v.x-objectArray[i].get()->box.getW()/2;
             y1=v.y-objectArray[i].get()->box.getH()/2;
             x2=v.x-objectArray[i].get()->box.getW()/2;
             y2=v.y+objectArray[i].get()->box.getH()/2;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x+objectArray[i].get()->box.getW()/2;
            y1=v.y-objectArray[i].get()->box.getH()/2;
            x2=v.x+objectArray[i].get()->box.getW()/2;
            y2=v.y+objectArray[i].get()->box.getH()/2;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x-objectArray[i].get()->box.getW()/2;
            y1=v.y+objectArray[i].get()->box.getH()/2;
            x2=v.x+objectArray[i].get()->box.getW()/2;
            y2=v.y+objectArray[i].get()->box.getH()/2;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x-objectArray[i].get()->box.getW()/2;
            y1=v.y-objectArray[i].get()->box.getH()/2;
            x2=v.x+objectArray[i].get()->box.getW()/2;
            y2=v.y-objectArray[i].get()->box.getH()/2;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);*/
        }

        tileMap->RenderLayer(3,120-Camera::pos.x*1.15, -Camera::pos.y*1.15);

        for(int i=0;i<objectArray_high.size();i++){
           this->objectArray_high[i].get()->Render();
        }


        tileMap->RenderLayer(4,120-Camera::pos.x*1.15, -Camera::pos.y*1.15);
        //HUD
        hud.Render();//renderizou apenas o nome
    }



void FightState::Resume(){}

void FightState::Pause(){}


std::vector<int> FightState::Gerar_Dominos(std::vector<int> mapa){

        //dominó
        int domino[12];
        domino[2]=domino[5]=domino[8]=domino[11]=0;
        domino[0]=Map::domino+rand()%6;
        domino[1]=Map::domino+rand()%6+24;
        domino[3]=Map::domino+rand()%6+6;
        domino[4]=Map::domino+rand()%6+24+6;
        domino[6]=Map::domino+rand()%6+12;
        domino[7]=Map::domino+rand()%6+24+12;
        domino[9]=Map::domino+rand()%6+18;
        domino[10]=Map::domino+rand()%6+24+18;
        Vec2 pos;
        pos.x=0;
        pos.y=0;
        int d=rand()%12;
        while(d%3!=0)
            d++;

        for(int i=0;i<4;i++){
            if(d>11)
                d=0;

            switch(i){
                case 0: pos.x=rand()%10;
                        pos.y=rand()%8;
                        while(pos.x<1)
                            pos.x=rand()%10;
                        while(pos.y<1)
                            pos.y=rand()%8;
                        break;

                case 1: pos.x=rand()%19;
                        pos.y=rand()%8;
                        while(pos.x<10)
                            pos.x=rand()%19;
                        while(pos.y<1)
                            pos.y=rand()%8;
                        break;

                case 2: pos.x=rand()%8;
                        pos.y=rand()%15;
                        while(pos.x<1)
                            pos.x=rand()%10;
                        while(pos.y<10)
                            pos.y=rand()%15;
                       break;

                case 3: pos.x=rand()%19;
                        pos.y=rand()%15;
                        while(pos.x<10)
                            pos.x=rand()%19;
                        while(pos.y<10)
                            pos.y=rand()%15;
                        break;
                default: break;
            }

            mapa.at(pos.x+pos.y*20+layer*3)=domino[d];
            mapa.at(pos.x+pos.y*20+layer*2+Map::under)=domino[d+1];
            mapa.at(pos.x+pos.y*20+layer  +Map::under+Map::under)=domino[d+2];
            d+=3;
        }

return mapa;
}

std::vector<int> FightState::Gerar_Legos(std::vector<int> mapa){

        //legos
        int legos[18];
        legos[0]=Map::peca1;    legos[1]=Map::peca1+1;
        legos[2]=Map::peca1+24; legos[3]=Map::peca1+1+24;
        legos[4]=3;legos[5]=4;

        legos[6]=Map::peca2;    legos[7]=Map::peca2+1;
        legos[8]=Map::peca2+24; legos[9]=Map::peca2+1+24;
        legos[10]=2;legos[11]=2;

        legos[12]=Map::peca3;    legos[13]=Map::peca3+1;
        legos[14]=Map::peca3+24; legos[15]=Map::peca3+1+24;
        legos[16]=3;legos[17]=4;

        Vec2 pos;
        pos.x=0;
        pos.y=0;
        int d=rand()%18;
        while(d%6!=0)
            d++;

        for(int i=0;i<3;i++){
            if(d>17)
                d=0;

            switch(i){
                case 0: pos.x=rand()%10;        //top-left
                        pos.y=rand()%8;
                        while(pos.x<1)
                            pos.x=rand()%10;
                        while(pos.y<1)
                            pos.y=rand()%8;
                        break;

                case 1: pos.x=rand()%18;        //top-rigth
                        pos.y=rand()%8;
                        while(pos.x<10)
                            pos.x=rand()%18;
                        while(pos.y<1)
                            pos.y=rand()%8;
                        break;

                case 2: pos.x=rand()%8;         //botton-left
                        pos.y=rand()%15;
                        while(pos.x<1)
                            pos.x=rand()%10;
                        while(pos.y<10)
                            pos.y=rand()%15;
                       break;

                case 3: pos.x=rand()%18;        //botton-rigth
                        pos.y=rand()%15;
                        while(pos.x<10)
                            pos.x=rand()%18;
                        while(pos.y<10)
                            pos.y=rand()%15;
                        break;
                default: break;
            }
         if( mapa.at(pos.x+pos.y*20+layer*3)==23){
            mapa.at(pos.x+pos.y*20+layer*3)=legos[d];
            mapa.at(pos.x+pos.y*20+layer*3+1)=legos[d+1];
            mapa.at(pos.x+pos.y*20+layer*2+Map::under)=legos[d+2];
            mapa.at(pos.x+pos.y*20+layer*2+Map::under+1)=legos[d+3];
            mapa.at(pos.x+pos.y*20+layer  +Map::under+Map::under)=legos[d+4];
            mapa.at(pos.x+pos.y*20+layer  +Map::under+Map::under+1)=legos[d+5];
            d+=6;
        }
        else
            i--;
        }

return mapa;
}

std::vector<int> FightState::Gerar_Cartas(std::vector<int> mapa){

        //legos
        int cartas[8];
        cartas[0]=cartas[2]=cartas[4]=cartas[6]=Map::carta;
        cartas[1]=cartas[3]=cartas[5]=cartas[7]=Map::carta+24;


        Vec2 pos;
        pos.x=0;
        pos.y=0;
        int d=rand()%8;
        while(d%2!=0)
            d++;

        for(int i=0;i<4;i++){
            if(d>7)
                d=0;

            switch(i){
                case 0: pos.x=rand()%10;        //top-left
                        pos.y=rand()%8;
                        while(pos.x<1)
                            pos.x=rand()%10;
                        while(pos.y<1)
                            pos.y=rand()%8;
                        break;

                case 1: pos.x=rand()%18;        //top-rigth
                        pos.y=rand()%8;
                        while(pos.x<10)
                            pos.x=rand()%18;
                        while(pos.y<1)
                            pos.y=rand()%8;
                        break;

                case 2: pos.x=rand()%8;         //botton-left
                        pos.y=rand()%15;
                        while(pos.x<1)
                            pos.x=rand()%10;
                        while(pos.y<10)
                            pos.y=rand()%15;
                       break;

                case 3: pos.x=rand()%18;        //botton-rigth
                        pos.y=rand()%15;
                        while(pos.x<10)
                            pos.x=rand()%18;
                        while(pos.y<10)
                            pos.y=rand()%15;
                        break;
                default: break;
            }

            if( mapa.at(pos.x+pos.y*20+layer*3)==23){
                mapa.at(pos.x+pos.y*20+layer*3)=cartas[d];
                mapa.at(pos.x+pos.y*20+layer*2+Map::under)=cartas[d+1];
                d+=2;
            }
            else
                i--;
        }

return mapa;
}


//adiciona todos os sprites válidos na lista de objetos
void FightState::Layer2Obj(int layer,std::vector<int> mapa){

int xx,yy,tile;
    for( yy=0;yy<tileMap->GetHeight();yy++){
        for(xx=0;xx<tileMap->GetWidth();xx++){
            tile=tileMap->At(xx,yy,layer);

            if (tile!=23){
                AddObject(new Objetos(260+xx*tile_side*escala,10+yy*tile_side*escala));//...20+yy*...

            }

        }
    }
}




