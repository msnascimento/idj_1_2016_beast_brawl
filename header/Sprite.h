#ifndef HEADER_SPRITE
#define HEADER_SPRITE
#include "GameException.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#define PI 3.1415926535
#define renderizar  Render(this->box.getX()-(int)Camera::pos.x-sp.GetWidth()/2, this->box.getY()-(int)Camera::pos.y-sp.GetHeight()/2,rotation);


class Sprite{

public:
    Sprite();

    Sprite(std::string file,int frameCount  = 1,float frameTime  = 1);

    ~Sprite();

    void Open(std::string file);

    void SetClip(int x,int y, int w, int h);

    void Render(int x, int y,float angle = 0);

    int GetWidth();

    int GetHeight();

    bool IsOpen(){
        if(texture!=nullptr)return true;else return false;
    }

    void SetScaleX (float scale);
    void SetScaleY (float scale);
    float GetScaleX();
    float GetScaleY();

   void Update (float dt);
    void SetFrame (int frame);
    void SetFrameCount (int frameCount);
    void SetFrameTime (float frameTime);


private:

SDL_Texture* texture;
int width;
int height;
float scaleX;
float scaleY;
SDL_Rect clipRect;
std::string file;
    int frameCount;
    int currentFrame;
    float timeElapsed;
    float frameTime;
    };
#endif
