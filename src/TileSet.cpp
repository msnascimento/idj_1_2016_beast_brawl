#include "../header/TileSet.h"


    TileSet::TileSet(int tileWidth, int tileHeight, std::string file, float fator){
    this->tileWidth=tileWidth;
    this->tileHeight=tileHeight;


    Sprite tileset=*(new Sprite(file));

    tileset.SetScaleX(fator);
    tileset.SetScaleY(fator);
    tileset.SetClip(0,0,tileset.GetWidth(),tileset.GetHeight());

     TileSet::tileSet= tileset;
    //tileSet.Open(file);
      if(tileSet.IsOpen()){
          rows= tileSet.GetHeight()/(tileHeight*fator);
          columns=tileSet.GetWidth()/(tileWidth*fator);
      }
  }

  void TileSet::Render(unsigned int index,float x, float y){
    if(index>=0 && index<TileSet::rows*TileSet::columns){
        //calcular a posição do sprite
        tileSet.SetClip((index%columns)*tileWidth,(index/columns)*tileHeight,tileWidth,tileHeight);
        tileSet.Render(x*tileSet.GetScaleX(),y*tileSet.GetScaleY());
    }
  }

  int TileSet::GetTileWidth(){return tileWidth;}
  int TileSet::GetTileHeight(){return tileHeight;}


