
#include "../header/TileSet.h"
#include "../header/TileMap.h"
#include "../header/GameException.h"
#include <iostream>
#include <fstream>

 TileMap::TileMap(std::string file,TileSet* tileSet){
    Load(file);
    TileMap::tileSet=tileSet;
}

  void TileMap::Load(std::string file){
    std::ifstream myfile;
    myfile.open(file,std::ios::in);

    if (!myfile.is_open()) {
        throw GameException("nao foi possível abrir o arquivo de TileMap\n");
    }

    char c='0';
    int largura,altura,profundidade,tile;
    myfile>>largura;
    myfile>>c;
    myfile>>altura;
    myfile>>c;
    myfile>>profundidade;
    myfile>>c;

    mapWidth=largura;
    mapHeight=altura;
    mapDepth=profundidade;

     for(int z=0;z<profundidade;z++){
        for(int y=0;y<largura;y++){
            for(int x=0;x<largura;x++){
                myfile>>tile;
                tileMatrix.push_back(tile);
                myfile>>c;
            }

        }
    }
    myfile.close();
}


void TileMap::Write(std::string file){
     std::ofstream myfile;
    myfile.open(file,std::ios::out);

    if (!myfile.is_open()) {
        throw GameException("nao foi possível abrir o arquivo de TileMap\n");
    }

    int x=0;
    myfile<<"24,11,1,\n\n";

    for(x=0;x<264;x++){
            if(x<10){
                myfile<<(char)(48+x);
                myfile<<",";
            }else if(x<100){
                myfile<<(char)(48+x/10);
                myfile<<(char)(48+x%10);
                myfile<<",";
            }else{
                myfile<<(char)(48+x/100);
                myfile<<(char)(48+(x%100)/10);
                myfile<<(char)(48+x%10);
                myfile<<",";
            }
        if((x+1)%24==0 && x!=0)
        myfile<<"\n";
    }

myfile.close();

}


  void TileMap::SetTileSet(TileSet* tileset){
    TileMap::tileSet=tileset;
}

  int& TileMap::At (int x, int y, int z){

    if(x>mapWidth-1 || x<0 ||y>mapHeight-1 ||y<0 ||z>mapDepth-1 ||z<0)
        throw GameException("indice tile errado\n");

    int indice=x+y*mapWidth+z*mapWidth*mapHeight;

    return TileMap::tileMatrix.at(indice);

  }

  void TileMap::RenderLayer(int layer,int cameraX, int cameraY){
    //int x=0,y=0;
int xx=0,yy=0;
int tile,largura,altura;


    for(xx=0;xx<mapWidth;xx++){
        for( yy=0;yy<mapHeight;yy++){
            tile=TileMap::At(xx,yy,layer);
            largura=xx*tileSet->GetTileWidth();
            altura=yy*tileSet->GetTileHeight();
            if(tile>-1){
                tileSet->Render(tile,largura+cameraX,altura+cameraY);
            }
        }
    }
}

  void TileMap::Render(int cameraX, int cameraY){

    for(int i=0;i<mapDepth;i++){
        TileMap::RenderLayer(i,cameraX,cameraY);
    }
}


  int TileMap::GetWidth(){return mapWidth;}
  int TileMap::GetHeight(){return mapHeight;}
  int TileMap::GetDepth(){return mapDepth;}
