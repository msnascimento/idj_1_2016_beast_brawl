
#ifndef HEADER_ESTATE
#define HEADER_ESTATE

#include "Music.h"
#include "State.h"
#include "Sprite.h"
#include "Text.h"
#include "Timer.h"
#include "StateData.h"

class EndState: public State{


public:
    EndState(StateData stateData);
    ~EndState();

    void Update();
    void Render();
    void Pause();
    void Resume();

private:

    Music* music;
    Sprite bg;
    Text* instruction;

    Timer* time_texto;
    bool mostrar_texto;
};
#endif
