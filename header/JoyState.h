
#ifndef HEADER_JSTATE
#define HEADER_JSTATE

#include "Music.h"
#include "State.h"
#include "Sprite.h"
#include "Text.h"
#include "Timer.h"
#include "Sound.h"
#include "JoyStick.h"


class JoyState: public State{


public:
    JoyState();


    void Update();
    void Render();
    void Pause();
    void Resume();

private:

    bool press[4][MAX_BUTTONS];
    Text* R1;
    Text* R2;
    Text* L1;
    Text* L2;

    Text* P1;
    Text* P2;
    Text* P3;
    Text* P4;

    Text* Atualizar;
    Text* Voltar;


    Sprite bg;
    Sprite sp;

    std::vector<Sprite> buttons;

    Sound* opcao;
};

#endif
