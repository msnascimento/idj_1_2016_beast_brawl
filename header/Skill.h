#ifndef SKILL_H
#define	SKILL_H

#include "GameObject.h"
#include "Timer.h"

#define SHIELD_CONTACT_SOUND "audio/Sons/WAV/Hit2.wav"

using namespace std;

class Skill : public GameObject{
public:
    Skill();
    virtual ~Skill();
    virtual void Update(float dt);
    virtual void Render();
    virtual bool IsDead();
    virtual void NotifyCollision (GameObject& other);
    virtual bool Is (std::string type);
    virtual Skill* NewInstance(Vec2 orig, float angle, int source);
    float cooldown;
    float cdStamp;
};

#endif	/* SKILL_H */

