#ifndef HEADER_TMAP
#define HEADER_TMAP

#include"TileSet.h"
#include <vector>

class TileMap{

public:
      TileMap(std::string file,TileSet* tileSet);

      void Load(std::string file);
      void Write(std::string file);
      void SetTileSet(TileSet* tileset);

      int& At (int x, int y, int z=0);
      void RenderLayer(int layer,int cameraX=0, int cameraY=0);
      void Render(int cameraX=0, int cameraY=0);
      int GetWidth();
      int GetHeight();
      int GetDepth();
      std::vector<int> GetMap(){
            return tileMatrix;
      }
    void SetMap(std::vector<int> mapa){
        tileMatrix=mapa;
      }

private:
      std::vector<int> tileMatrix;
      TileSet* tileSet;
      int mapWidth;
      int mapHeight;
      int mapDepth;
};

#endif
