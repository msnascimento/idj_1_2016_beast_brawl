#include "../header/Camera.h"
#include "../header/Vec2.h"
#include "../header/InputManager.h"
#include <SDL2/SDL.h>

Vec2 Camera::pos;
Vec2 Camera::speed;

GameObject* Camera::focus;
void Camera::Follow (GameObject* newFocus){focus=newFocus;}
void Camera::Unfollow (){Camera::focus=nullptr;}

void Camera::Update (float dt){

    if( InputManager::GetInstance().IsKeyDown(SDLK_LEFT)){
        pos.x-=dt*speed.x;
    }

    if(InputManager::GetInstance().IsKeyDown(SDLK_RIGHT)){
        pos.x+=dt*speed.x;
    }

     if(InputManager::GetInstance().IsKeyDown(SDLK_UP)){
        pos.y-=dt*speed.y;
    }

     if(InputManager::GetInstance().IsKeyDown(SDLK_DOWN)){
         pos.y+=dt*speed.y;
    }


    if (focus!=nullptr){
       Vec2 posicao=focus->box.GetCenter();
       pos.x=posicao.x-1024/2;
       pos.y=posicao.y-600/2;
    }

}

