#include "../header/UI.h"
#include "../header/Game.h"
#include <iostream>
UI::UI()
{
}

UI::UI(int id, UI* outer)
{
    this->id = id;
    snap = Snap::UNDEFINED;
    SetOuterUI(outer);
}

UI::UI(int id, float  w, float h, Snap snap, UI* outer)
{
    this->snap = snap;
    this->id = id;

    SetOuterUI(outer);
    SetSnap(snap);

    box.w = w;
    box.h = h;
}

UI::UI(int id, float w, float h, float  pos, Snap snap, UI* outer)
{
    this->snap = snap;
    this->id = id;

    SetOuterUI(outer);
    SetSnap(pos, snap);

    box.w = w;
    box.h = h;

}

UI::~UI()
{
    //dtor
}

void UI::SetSnap(float pos, Snap snap)
{
    Vec2 windowSize = Game::GetInstance().GetDisplay();
    this->snap = snap;
    if(outer == nullptr)
    {
        switch(snap)
        {
            case Snap::LEFT:
                box.x = 0;
                box.y = pos;
                break;
            case Snap::RIGHT:
                box.x = windowSize.x - box.w;
                box.y = pos;
                break;
            case Snap::UP:
                box.x = pos;
                box.y = 0;
                break;
            case Snap::DOWN:
                box.x = pos;
                box.y = windowSize.y - box.h;
                break;
            default:    //Se for passado os outros tipos de snap de forma acidental, assume-se que a intenção era usar o outro SetSnap
                SetSnap(snap);
                break;
        }
    }
    else
    {
        switch(snap)
        {
            case Snap::LEFT:
                box.x = 0;
                box.y = pos;
                break;
            case Snap::RIGHT:
                box.x = outer->box.w - box.w;
                box.y = pos;
                break;
            case Snap::UP:
                box.x = pos;
                box.y = 0;
                break;
            case Snap::DOWN:
                box.x = pos;
                box.y = outer->box.h - box.h;
                break;
            default:    //Se for passado os outros tipos de snap de forma acidental, assume-se que a intenção era usar o outro SetSnap
                SetSnap(snap);
                break;
        }
        box.x+=outer->box.x;
        box.y+=outer->box.y;
    }
}

void UI::SetSnap(Snap snap)
{
    Vec2 windowSize = Game::GetInstance().GetDisplay();
    this->snap = snap;
    if(outer == nullptr)
    {
        switch(snap)
        {
            case Snap::CENTER:
                box.x = (windowSize.x/2) - box.w/2;
                box.y = (windowSize.y/2) - box.h/2;
                break;
            case Snap::CENTERLEFT:
                box.x = 0;
                box.y = (windowSize.y/2) - box.h/2;
                break;
            case Snap::CENTERRIGHT:
                box.x = windowSize.x - box.w;
                box.y = (windowSize.y/2) - box.h/2;
                break;
            case Snap::CENTERUP:
                box.x = (windowSize.x/2) - box.w/2;
                box.y = 0;
                break;
            case Snap::CENTERDOWN:
                box.x = (windowSize.x/2) - box.w/2;
                box.y = windowSize.y - box.h;
                break;
            case Snap::TOPLEFT:
                box.x = 0;
                box.y = 0;
                break;
            case Snap::TOPRIGHT:
                box.x = windowSize.x-box.w;
                box.y = 0;
                break;
            case Snap::BOTTOMLEFT:
                box.x = 0;
                box.y = windowSize.y-box.h;
                break;
            case Snap::BOTTOMRIGHT:
                box.x = windowSize.x-box.w;
                box.y = windowSize.y-box.h;
                break;
            default:
                std::cout<<"Erro em UI::SetSnap(Snap): valor invalido de snap"<<std::endl;
                break;
        }
    }
    else
    {
        switch(snap)
        {
            case Snap::CENTER:
                box.x = (outer->box.w/2) - box.w/2;
                box.y = (outer->box.h/2) - box.h/2;
                break;
            case Snap::CENTERLEFT:
                box.x = 0;
                box.y = (outer->box.h/2) - box.h/2;
                break;
            case Snap::CENTERRIGHT:
                box.x = outer->box.w - box.w;
                box.y = (outer->box.h/2) - box.h/2;
                break;
            case Snap::CENTERUP:
                box.x = (outer->box.w/2) - box.w/2;
                box.y = 0;
                break;
            case Snap::CENTERDOWN:
                box.x = (outer->box.w/2) - box.w/2;
                box.y = outer->box.h - box.h;
                break;
            case Snap::TOPLEFT:
                box.x = 0;
                box.y = 0;
                break;
            case Snap::TOPRIGHT:
                box.x = outer->box.w-box.w;
                box.y = 0;
                break;
            case Snap::BOTTOMLEFT:
                box.x = 0;
                box.y = outer->box.h-box.h;
                break;
            case Snap::BOTTOMRIGHT:
                box.x = outer->box.w-box.w;
                box.y = outer->box.h-box.h;
                break;
            default:
                std::cout<<"Erro em UI::SetSnap(Snap): valor invalido de snap"<<std::endl;
                break;
        }
        box.x+=outer->box.x;
        box.y+=outer->box.y;
    }
}

void UI::SetOuterUI(UI* outer)
{
    this->outer = outer;
    if(snap != Snap::UNDEFINED)
    {
        SetSnap(snap);
    }
}

UI* UI::GetOuterUI()
{
    return outer;
}


void UI::ClearOuter()
{
    outer = nullptr;
}

void UI::AddInnerUI(UI* inner)
{
    if(inner->outer == nullptr)
    {
        inner->outer = this;
    }
    std::shared_ptr<UI> ptr(inner);
    innerArray.emplace(inner->id, ptr);
}

void UI::RemoveInner(int id)
{
    innerArray.erase(id);
}

void UI::RenderInner()
{
    for (auto& it : innerArray)
    {
        it.second->Render();
    }
}

void UI::UpdateInner(float dt)
{
    for (auto& it : innerArray)
    {
        it.second->Update(dt);
    }
}

