#ifndef HEADER_STATE
#define HEADER_STATE

#include <vector>
#include <memory>
#include "Sprite.h"
#include "TileSet.h"
#include "TileMap.h"
#include "GameObject.h"
#include "UI.h"

class State{


public:
    State();
    virtual ~State();
    void virtual Update()=0;
    void virtual Render()=0;
    void virtual Pause ()=0;
    void virtual Resume () =0;
    void virtual AddObject(GameObject* ptr);
    void virtual AddObject_low(GameObject* ptr);
    void virtual AddObject_high(GameObject* ptr);
    bool QuitRequested();
    bool PopRequested();

protected:

    void virtual UpdateArray(float dt);
    void virtual RenderArray();

    virtual void UpdateUI(float dt);
    virtual void RenderUI();

    bool popRequested;
    bool quitRequested;

    std::vector<std::unique_ptr<GameObject>> objectArray;
    std::vector<std::unique_ptr<GameObject>> objectArray_low;
    std::vector<std::unique_ptr<GameObject>> objectArray_high;

    std::vector<std::unique_ptr<UI>> UIArray;
};
#endif
