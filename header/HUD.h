#ifndef HUD_H
#define HUD_H

#include <string>
#include "../header/UI.h"
#include "../header/PlayerCard.h"
#define HUDBG "img/hud/hudbg.png"

using namespace std;

class HUD : public UI
{
    public:
        HUD();
        virtual ~HUD();

        static HUD& GetInstance();

        void Update(float dt);
        void Render();

        void AddCard(int joynum, Snap snap, string img);   //Snap deve ser TOPLEFT(P1), TOPRIGHT(P2, BOTTOMLEFT(P3) ou BOTTOMRIGHT(P4)
        void SetHP(int joynum, float value);
        void SetElements(int joynum, int elements[3]);


    protected:
    private:
        static HUD* instance;
};

#endif // HUD_H
