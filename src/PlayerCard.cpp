#include "../header/PlayerCard.h"

PlayerCard::PlayerCard(int id, Snap snap, string img, UI* outer) : UI(-1, outer)
{
    this->id = id;
    //Abertura e tratamento da imagem de background
    sp.Open(img);
    box.w = sp.GetWidth();
    box.h = sp.GetHeight();
    sp.SetClip(0, 0, box.w, box.h);
    SetSnap(snap);

    //Elementos
    //Y = 44px
    //X:
    //Primeiro orb em 86px
    //Segundo 108
    //terceiro 130
    for(int i =0; i<3; i++)
    {
        orbs_sprite[i].Open(BLUEORB);
        orbsbox[i].w = orbs_sprite[i].GetWidth();
        orbsbox[i].h = orbs_sprite[i].GetHeight();
        orbs_sprite[i].SetClip(0, 0, orbsbox[i].w, orbsbox[i].h);
        orbsbox[i].y = 44+box.y;
    }
    orbsbox[0].x = 86+box.x;
    orbsbox[1].x = 108+box.x;
    orbsbox[2].x = 130+box.x;

    //Tratamento da barra de hp
    hpsp.Open(CARD_HPIMG);
    hpbox.w = box.w;
    hpbox.h = box.h;
    hpsp.SetClip(0, 0, hpbox.w, hpbox.h);
    //Posicionado em baixo da imagem
    hpbox.x = box.x;
    hpbox.y = box.y;
}

PlayerCard::~PlayerCard()
{
    //dtor
}

void PlayerCard::SetHP(float value)
{
    hpsp.SetClip(0, 0, 70+ (value*152), hpbox.h);
}

void PlayerCard::SetElements(int elements[3])
{
    string fname;

    for(int i=0; i<3; i++)
    {
        switch(elements[i])
        {
            case 0:
                fname = "img/hud/orbs/blue.png";
                break;
            case 1:
                fname = "img/hud/orbs/green.png";
                break;
            case 2:
                fname = "img/hud/orbs/yellow.png";
                break;
        }
        orbs_sprite[i].Open(fname);
    }
}

void PlayerCard::Update(float dt)
{
}

void PlayerCard::Render()
{
    hpsp.Render(hpbox.x, hpbox.y);  //Barra de HP
    for(int i = 0; i<3; i++)
    {
        orbs_sprite[i].Render(orbsbox[i].x, orbsbox[i].y);
    }
    sp.Render(box.x, box.y);  //background
}
