#include <memory>
#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

#include "../header/Game.h"
#include "../header/State.h"
#include "../header/TitleState.h"
#include "../header/InputManager.h"
#include "../header/Resources.h"
#include "../header/GameException.h"
#include "../header/JoyState.h"
#include "../header/FightState.h"

Game* Game::instance=nullptr;


Game::Game(std::string title,int width,int height){


    if(Game::instance!=nullptr)
        throw SDLException("1");
    else
        Game::instance=this;




    /*iniciliaza a SDL*/
    if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER|SDL_INIT_JOYSTICK)<0)
    {
        throw SDLException("2");
    }else{

        if(SDL_NumJoysticks()>0){
        // inicializa joysticks
            SDL_JoystickEventState(SDL_ENABLE);
            printf("%i joysticks:\n", SDL_NumJoysticks() );

            InputManager::GetInstance().SetJoy(SDL_NumJoysticks());

            for(int i=0; i < InputManager::GetInstance().GetJoy(); i++ ){
                SDL_Joystick *joystick = SDL_JoystickOpen(i);

                InputManager::GetInstance().AddJoyStick(new JoyStick(joystick));
                printf("%s\n", SDL_JoystickName(joystick));
            }
            printf("\n");
        }else
        //sem joysticks
             printf("Nenhum joystick encontrado\n\n");
    }

    if(IMG_Init(IMG_INIT_JPG|IMG_INIT_PNG)<0)
    {
        SDL_Quit();
        throw SDLException("3");
    }
    printf("IMG_Init: %s\n", IMG_GetError());

    if(TTF_Init() <0){
        IMG_Quit();
        SDL_Quit();
        throw SDLException("4");
    }


    if(Mix_Init(MIX_INIT_OGG|MIX_INIT_MP3)<0){

        IMG_Quit();
        TTF_Quit();
        SDL_Quit();
        throw GameException(Mix_GetError());
        // handle error
    }

    if(Mix_OpenAudio(MIX_DEFAULT_FREQUENCY,MIX_DEFAULT_FORMAT,MIX_DEFAULT_CHANNELS,1024)<0)
    {
        IMG_Quit();
        TTF_Quit();
        Mix_Quit();
        SDL_Quit();
        throw SDLException("6");
    }


    Game::window= SDL_CreateWindow(title.data(),SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,width,height,0);
    Game::renderer  = SDL_CreateRenderer(window,-1,SDL_RENDERER_PRESENTVSYNC);
    if (window==nullptr || renderer==nullptr)
    {
        SDL_Quit();
        IMG_Quit();
        throw SDLException("7");
    }

    dt=0.0;
    frameStart=SDL_GetTicks ();
    storedState =nullptr;
    display.x=width;
    display.y=height;
}


Vec2 Game::GetDisplay(){

return display;
}

Game::~Game(){
    IMG_Quit();
    TTF_Quit();
    Mix_CloseAudio();
    Mix_Quit();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();


    while (!stateStack.empty()){
        stateStack.pop();
    }


}

void Game::Run(){

    State* atual;
    TitleState* st=new TitleState();

    atual=st;
    stateStack.emplace(st);

    while (atual->QuitRequested()==false){
        if(stateStack.empty())
            return;

        State* atual =stateStack.top().get();

        if( atual==nullptr)
            return;

        CalculateDeltaTime();
        InputManager::GetInstance().Update();
        atual->Update();//atualiza tudo
        atual->Render();//renderiza tudo
        SDL_RenderPresent(renderer);//mostra


        if(atual->PopRequested()){
            stateStack.pop();

            atual =stateStack.top().get();
            if( atual==nullptr)
                return;

            atual->Resume();
        }

        if(storedState!=nullptr){
            stateStack.top().get()->Pause();

            stateStack.emplace(storedState);
            storedState=nullptr;
        }
        // SDL_Delay(33);//espera
    }
}



SDL_Renderer* Game::GetRenderer(){
    return renderer;
}

State& Game::GetCurrentState(){
        return *stateStack.top().get();
}

 void Game::Push(State* state){

storedState=state;

 }

Game& Game::GetInstance(){
    return *instance;
}

float Game::GetDeltaTime(){
    return dt;
}

void Game::CalculateDeltaTime(){
    int s=frameStart;
    frameStart=SDL_GetTicks ();
    dt=(float)(frameStart-s)/1000.0;
}



int main(int argc,char**argv){

    try
    {
        printf("começou\n");
        Game* jogo = new Game("Beast Brawl",1280,720);
        jogo->Run();

        Resources::ClearImages();
        Resources::ClearMusic();
        Resources::ClearSound();
        Resources::ClearFont();

        printf("terminou\n");
    }
    catch(GameException &x)
    {
        printf("\n\nGameException:%s\n\n",x.what());
    }
    catch(SDLException &x)
    {
        printf("\n\nSDLException:%s %s\n\n",x.what() , SDL_GetError());
    }

}

