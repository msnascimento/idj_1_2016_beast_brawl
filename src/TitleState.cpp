

#include "../header/TitleState.h"
#include "../header/InputManager.h"
#include "../header/Game.h"
#include "../header/Camera.h"
#include "../header/FightState.h"

#include "../header/SelectState.h"
#include "../header/JoyState.h"

    TitleState::TitleState(){
        TitleState::bg= *(new Sprite("img/hud/menuescalado.png"));
        bg.SetClip(0,0,1280,720);

        SDL_Color cor;
        cor.a=255;
        cor.r=255;
        cor.g=100;
        cor.b=100;

        iniciar= new Text(FONT,36,SOLID,"Jogar",cor,0,0);
        input= new Text(FONT,30,SOLID,"Configurar joystick",cor,0,0);
        sair= new Text(FONT,36,SOLID,"Sair",cor,0,0);

        iniciar->SetPos(Game::GetInstance().GetDisplay().x/2,Game::GetInstance().GetDisplay().y/5.2);
        input->SetPos(Game::GetInstance().GetDisplay().x/2,Game::GetInstance().GetDisplay().y/3.8);
        sair->SetPos(Game::GetInstance().GetDisplay().x/1.3,Game::GetInstance().GetDisplay().y/1.4);

        music= new Music();
        music->Open("audio/Musicas/Possible_BGM2.wav");
        music->Play(-1);
        opcao= new Sound();
        opcao->Open("audio/Sons/WAV/Select1.wav");
    }


    void TitleState::Update(){

    if(InputManager::GetInstance().KeyPress(SDLK_ESCAPE))
           this->quitRequested=true;
    if(InputManager::GetInstance().QuitRequested())
           this->quitRequested=true;


        SDL_Color cor;
        cor.a=255;cor.r=255;cor.g=255;cor.b=255;
        SDL_Color cor2;
        cor2.a=255;cor2.r=150;cor2.g=0;cor2.b=0;

    if(iniciar->IsMouse_TextInside()){
        iniciar->SetColor(cor);
        if(InputManager::GetInstance().IsMouseDown(LEFT_MOUSE_BUTTON)){
            opcao->Play(1);
            Game::GetInstance().Push(new SelectState());
        }
    }else
        iniciar->SetColor(cor2);

    if(input->IsMouse_TextInside()){
            input->SetColor(cor);
            if(InputManager::GetInstance().IsMouseDown(LEFT_MOUSE_BUTTON)){
            opcao->Play(1);
                Game::GetInstance().Push(new JoyState());
            }
    }else
        input->SetColor(cor2);

    if(sair->IsMouse_TextInside()){
            sair->SetColor(cor);
            if(InputManager::GetInstance().IsMouseDown(LEFT_MOUSE_BUTTON)){
                opcao->Play(1);
                quitRequested=true;
            }
    }else
        sair->SetColor(cor2);


    }


    void TitleState::Render(){

        bg.Render(0,0);


            iniciar->Render(Camera::pos.x,Camera::pos.y);
            input->Render(Camera::pos.x,Camera::pos.y);
            sair->Render(Camera::pos.x,Camera::pos.y);


          /*   Vec2 v;
    int x1,x2,y1,y2;

            v=Vec2(iniciar->GetBox().getX()-Camera::pos.x,iniciar->GetBox().getY()-Camera::pos.y);
            x1=v.x;
            y1=v.y;
            x2=v.x+iniciar->GetBox().getW();
            y2=v.y;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x;
            y1=v.y+iniciar->GetBox().getH();
            x2=v.x+iniciar->GetBox().getW();
            y2=v.y+iniciar->GetBox().getH();
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x;
            y1=v.y;
            x2=v.x;
            y2=v.y+iniciar->GetBox().getH();;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x+iniciar->GetBox().getW();;
            y1=v.y;
            x2=v.x+iniciar->GetBox().getW();;
            y2=v.y+iniciar->GetBox().getH();;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);


            v=Vec2(input->GetBox().getX()-Camera::pos.x,input->GetBox().getY()-Camera::pos.y);
            x1=v.x;
            y1=v.y;
            x2=v.x+input->GetBox().getW();
            y2=v.y;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x;
            y1=v.y+input->GetBox().getH();
            x2=v.x+input->GetBox().getW();
            y2=v.y+input->GetBox().getH();
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x;
            y1=v.y;
            x2=v.x;
            y2=v.y+input->GetBox().getH();;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x+input->GetBox().getW();;
            y1=v.y;
            x2=v.x+input->GetBox().getW();;
            y2=v.y+input->GetBox().getH();;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);


            v=Vec2(sair->GetBox().getX()-Camera::pos.x,sair->GetBox().getY()-Camera::pos.y);
            x1=v.x;
            y1=v.y;
            x2=v.x+sair->GetBox().getW();
            y2=v.y;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x;
            y1=v.y+sair->GetBox().getH();
            x2=v.x+sair->GetBox().getW();
            y2=v.y+sair->GetBox().getH();
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x;
            y1=v.y;
            x2=v.x;
            y2=v.y+sair->GetBox().getH();;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);
            x1=v.x+sair->GetBox().getW();;
            y1=v.y;
            x2=v.x+sair->GetBox().getW();;
            y2=v.y+sair->GetBox().getH();;
            SDL_RenderDrawLine(Game::GetInstance().GetRenderer(), x1,y1,x2,y2);*/
    }


    void TitleState::Pause(){

    }

    void TitleState::Resume(){

    }

