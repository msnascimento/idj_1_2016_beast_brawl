#ifndef HEADER_SOUND
#define HEADER_SOUND

#include <string>
#include <SDL2/SDL_mixer.h>

class Sound{


public:
    Sound();
    Sound(std::string file);
    void Play (int times);
    void Stop ();
    void Open (std::string file);
    bool IsOpen ();

 private:
    Mix_Chunk*  chunk;
    int channel;

};
#endif
