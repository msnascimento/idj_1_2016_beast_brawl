
#include "../header/Animation.h"
#include "../header/InputManager.h"
#include "../header/Camera.h"
#include "../header/Game.h"
#include "../header/Sound.h"
#include "../header/Player.h"
#include "../header/JoyStick.h"
#include "../header/HUD.h"
#include <string>
#include <iostream>
#include <SDL2/SDL.h>

#define COOLDOWN 0.75
#define MAXSPEED 300


    Player::Player (float x,float y,std::string personagem, int nPlayer){
        rotation = 0;
        int i;

        this->sp_front= *(new Sprite("img/personagens/"+personagem+"_front.png",4,0.3));
        sp_front.SetScaleX(2);
        sp_front.SetScaleY(2);

        if(personagem=="rhino")
            this->box.SetRect(x,y,sp_front.GetWidth()-18,sp_front.GetHeight()/3+10);
        else
            this->box.SetRect(x,y,sp_front.GetWidth()-8,sp_front.GetHeight()/3+10);//-5

        this->sp_left= *(new Sprite("img/personagens/"+personagem+"_left.png",4,0.3));
        sp_left.SetScaleX(2);
        sp_left.SetScaleY(2);

        this->sp_back= *(new Sprite("img/personagens/"+personagem+"_back.png",4,0.3));
        sp_back.SetScaleX(2);
        sp_back.SetScaleY(2);


        this->sp_right= *(new Sprite("img/personagens/"+personagem+"_right.png",4,0.3));
        sp_right.SetScaleX(2);
        sp_right.SetScaleY(2);

        player=this;

        speed.x=150;
        speed.y=150;
        linearSpeed=0.0;
        hp=MAX_HP;
        angle = PI/2;

        if(nPlayer==0)
            this->side=0;
        else if(nPlayer==1)
            this->side=3;
        else if(nPlayer==2)
            this->side=1;
        else if(nPlayer==3)
            this->side=2;

        for(i=0;i<4;i++)
            colisao[i]=false;


        this->nPlayer=nPlayer;
        this->player_name=personagem;
        dash.Restart();

    //Skills
    skillcount = 0;
    for(i=0; i<3; i++)
    {
        elements[i] = 0;
    }
    HUD::GetInstance().SetElements(nPlayer, elements);
    activeSkill = 0;

    AddSkill(new Projectile());
    AddSkill(new Banana());
    AddSkill(new BindSkill());
    AddSkill(new Boxe());
    AddSkill(new Shield());
    AddSkill(new Projectile());
    AddSkill(new Banana());
    AddSkill(new BindSkill());
    AddSkill(new Boxe());
    AddSkill(new Shield());

    state = State::NORMAL;

    bindstamp = 0;
    slidestamp = 0;

    side_left=false;
    side_down=false;
    side_right=false;
    side_up=false;
    key=true;
    //Fim Skills

    som_dash = new Sound();
    som_dash->Open("audio/Sons/WAV/Dash.wav");
   }


     Player::~Player (){

        player=nullptr;
     }

    void  Player::Update (float dt){
        if(!parado){
            sp_front.Update(dt);
            sp_right.Update(dt);
            sp_back.Update(dt);
            sp_left.Update(dt);
        }

        parado=false;



///Mecanica da skill de prender

        //Verifica se a duração da skill passou para restaurar o movimento

        if(state == State::BINDED){
            if(bindstamp <= cd.Get())
            {
                state = State::NORMAL;
                speed.x = altspeed.x;
                speed.y = altspeed.y;
            }
        }

        if(state == State::SLIDE){
            if(slidestamp <= cd.Get()){
                state = State::NORMAL;
                speed.x = altspeed.x;
                speed.y = altspeed.y;
                 side_down=false;
                side_up=false;
                side_right=false;
                side_left=false;
                key=true;
                printf("parou\n");
            }else{
                switch(side){
                case 0: side_down=true; sp_back.SetFrame(0); break;
                 break;
                case 1: side_right=true; sp_right.SetFrame(2);break;
                     break;
                case 2: side_up=true;   sp_front.SetFrame(0);break;
                     break;
                case 3: side_left=true; sp_left.SetFrame(0);break;
                }
            printf("deslizando\n");
            }
        }
        if(state == State::BACK){
            if(backstamp <= cd.Get()){
                state = State::NORMAL;
                speed.x = altspeed.x;
                speed.y = altspeed.y;
                side_down=false;
                side_up=false;
                side_right=false;
                side_left=false;
                key=true;
                printf("parou\n");
            }else{
                switch(back_side){
                case 0:sp_back.SetFrame(0); break;

                case 1: sp_right.SetFrame(2);break;

                case 2: sp_front.SetFrame(0);break;

                case 3: sp_left.SetFrame(0);break;
                }
            printf("empurrado\n");
            }
        }



///Fim da Mecanica da skill de prender
        //baixo

        if((InputManager::GetInstance().IsJoyDown(nPlayer,6) && key)|| (InputManager::GetInstance().IsKeyDown(SDLK_DOWN) && key )|| (InputManager::GetInstance().GetAxisY(nPlayer) > 20000 && key ) || side_down){
            if(colisao[0]==false){
                this->box.setY(this->box.getY()+dt*speed.y);
                side=0;
            }

                //

            angle = PI/2;

            colisao[1]=false;
            colisao[2]=false;
            colisao[3]=false;
        }
           //direita
        else if((InputManager::GetInstance().IsJoyDown(nPlayer,5)&& key )|| (InputManager::GetInstance().IsKeyDown(SDLK_RIGHT)&& key ) || (InputManager::GetInstance().GetAxisX(nPlayer) > 20000 && key )|| side_right){
            if(colisao[1]==false){
                this->box.setX(this->box.getX()+dt*speed.x);
                side=1;
            }


            angle = 0;

            colisao[0]=false;
            colisao[2]=false;
            colisao[3]=false;

        }
        //cima
        else if((InputManager::GetInstance().IsJoyDown(nPlayer,4)&& key )|| (InputManager::GetInstance().IsKeyDown(SDLK_UP)&& key ) || (InputManager::GetInstance().GetAxisY(nPlayer) < -20000 && key )|| side_up){
            if(colisao[2]==false){
                this->box.setY(this->box.getY()-dt*speed.y);
                side=2;
            }


            angle = 3*PI/2;

            colisao[0]=false;
            colisao[1]=false;
            colisao[3]=false;
        }
        //esquerda
        else if((InputManager::GetInstance().IsJoyDown(nPlayer,7)&& key )|| (InputManager::GetInstance().IsKeyDown(SDLK_LEFT)&& key ) || (InputManager::GetInstance().GetAxisX(nPlayer) < -20000 && key ) || side_left){
            if(colisao[3]==false){
                this->box.setX(this->box.getX()-dt*speed.x);
                side=3;
            }


            angle = PI;

             colisao[0]=false;
            colisao[1]=false;
            colisao[2]=false;
        }
        else{
            parado=true;
        }

    if(InputManager::GetInstance().KeyPress(SDLK_1) || InputManager::GetInstance().JoyPress(nPlayer, JOY_SQUARE))
    {
        AddElement(0);
    }
    if(InputManager::GetInstance().KeyPress(SDLK_2) || InputManager::GetInstance().JoyPress(nPlayer, JOY_TRIANGLE))
    {
        AddElement(1);
    }
    if(InputManager::GetInstance().KeyPress(SDLK_3) || InputManager::GetInstance().JoyPress(nPlayer, JOY_CIRCLE))
    {
        AddElement(2);
    }
    if(InputManager::GetInstance().KeyPress(SDLK_SPACE) || InputManager::GetInstance().IsJoyDown(nPlayer, JOY_R1))
    {
        UseSkill();
    }
    if(InputManager::GetInstance().KeyPress(SDLK_d) || InputManager::GetInstance().JoyPress(nPlayer, JOY_L1))
    {
        if(dash.Get()>2.5){
            speed.x=300;
            speed.y=300;
            dash_rest.Restart();
            dash.Restart();
            Animation *poeira = new Animation(box.x, box.y, 0, "img/effect3.png", 7, 0.3, true);
            Game::GetInstance().GetCurrentState().AddObject_low(poeira);
            som_dash->Play(0);
        }
    }

    //cooldown
    cd.Update(dt);
    dash.Update(dt);
    dash_rest.Update(dt);
    this->box_projectile_colision.SetRect(this->box.getX()+1,this->box.getY()-10,sp_front.GetWidth(),sp_front.GetHeight()/1.4);

    //playerdeath->Update(Game::GetInstance().GetDeltaTime());
    if(dash_rest.Get()>0.5 && state!=BINDED){
        speed.x=150;
        speed.y=150;
    }

}


    void Player::Render (){

        if(state!=BACK){
            switch(side){
                case 0: sp_front.Render(this->box.getX()-(int)Camera::pos.x-sp_front.GetWidth()/2, this->box.getY()-(int)Camera::pos.y-sp_front.GetHeight()/2,rotation);
                        break;
                case 1: sp_right.Render(this->box.getX()-(int)Camera::pos.x-sp_front.GetWidth()/2, this->box.getY()-(int)Camera::pos.y-sp_right.GetHeight()/2,rotation);
                        break;
                case 2: sp_back.Render(this->box.getX()-(int)Camera::pos.x-sp_back.GetWidth()/2, this->box.getY()-(int)Camera::pos.y-sp_back.GetHeight()/2,rotation);
                        break;
                case 3: sp_left.Render(this->box.getX()-(int)Camera::pos.x-sp_left.GetWidth()/2, this->box.getY()-(int)Camera::pos.y-sp_left.GetHeight()/2,rotation);
                        break;
                }
        }else {
            switch(back_side){
                case 0: sp_front.Render(this->box.getX()-(int)Camera::pos.x-sp_front.GetWidth()/2, this->box.getY()-(int)Camera::pos.y-sp_front.GetHeight()/2,rotation);
                        break;
                case 1: sp_right.Render(this->box.getX()-(int)Camera::pos.x-sp_front.GetWidth()/2, this->box.getY()-(int)Camera::pos.y-sp_right.GetHeight()/2,rotation);
                        break;
                case 2: sp_back.Render(this->box.getX()-(int)Camera::pos.x-sp_back.GetWidth()/2, this->box.getY()-(int)Camera::pos.y-sp_back.GetHeight()/2,rotation);
                        break;
                case 3: sp_left.Render(this->box.getX()-(int)Camera::pos.x-sp_left.GetWidth()/2, this->box.getY()-(int)Camera::pos.y-sp_left.GetHeight()/2,rotation);
                        break;
                }
        }

    }

    bool  Player::IsDead (){

        if (hp<=0){
            //animaçao?
            return true;
            }
        else return false;
    }


    bool Player::Is(std::string type){

    if(type==player_name || type == "Player")
        return true;
    else return false;

    }

    void Player::NotifyCollision(GameObject& other){


        if(other.Is("Projectile")  && ((Projectile&)other).source != nPlayer)
        {
            hp-=((Projectile&)other).dmg;
            HUD::GetInstance().SetHP(nPlayer, (float)hp/MAX_HP);
        }
        if(other.Is("BindSkill") && ((Projectile&)other).source != nPlayer)
        {
            if(state != State::BINDED)
            {
                state = State::BINDED;
                bindstamp = cd.Get()+BIND_DURATION;
                //Salvando a speed para poder restaura-la apos a duração da skill
                altspeed.x = speed.x;
                altspeed.y = speed.y;
                speed.x = speed.y = 0;
            }
            else//Se o alvo ja esta preso, devemos apenas atualizar a duração
            {
                bindstamp = cd.Get()+BIND_DURATION;
            }
        }

        if(other.Is("Banana"))
        {
            if(state != State::SLIDE)
            {
                state = State::SLIDE;
                slidestamp = cd.Get()+SLIDE_DURATION;
                //Salvando a speed para poder restaura-la apos a duração da skill
                altspeed.x = speed.x;
                altspeed.y = speed.y;
                speed.x = speed.y = 50;
                key=false;
            }

            else//Se o alvo ja esta preso, devemos apenas atualizar a duração
            {
                bindstamp = cd.Get()+BIND_DURATION;
            }
        }

        if(other.Is("BoxeSkill")  && ((Projectile&)other).source != nPlayer)
        {
            if(state != State::BACK)
            {
              Boxe  b=(Boxe&)(other);
                state = State::BACK;
                backstamp = cd.Get()+BACK_DURATION;
                //Salvando a speed para poder restaura-la apos a duração da skill
                altspeed.x = speed.x;
                altspeed.y = speed.y;
                speed.x = speed.y = 200;
                key=false;
                if(b.getSide()==0){//0  \/
                    side_down=true;
                }else if(b.getSide()==1){
                    side_right=true;
                }else if(b.getSide()==2){//2  cima
                    side_up=true;
                }else{
                    side_left=true; //3  <-
                }
                back_side=side;
            }

            else//Se o alvo ja esta preso, devemos apenas atualizar a duração
            {
                backstamp = cd.Get()+BACK_DURATION;
            }
        }


       if(other.Is("Objetos"))
        {
            colisao[side]=true;
            Objetos b=(Objetos&)other;
            objeto_colisao= b.box.centro;
            switch(side){
                case 0:
                    box.setY(b.box.getY()-(b.box.getH())-1);
                    printf("colisao baixo\n"); break;
                case 1:
                    box.setX(b.box.getX()-(b.box.getW())-1);
                    printf("colisao direita\n");
                    break;
                case 2:
                    box.setY(b.box.getY()+(b.box.getH())+1);
                    printf("colisao cima\n");
                    break;
                case 3:
                    box.setX(b.box.getX()+(b.box.getW())+1);
                    printf("colisao esquerda\n");
                    break;
            }
            //parado=true;
        }

    }

void Player::AddSkill(Skill* skill)
{
    skills.push_back(skill);
    skillcount++;
}

void Player::AddElement(int element)
{
    elements[2] = elements[1];
    elements[1] = elements[0];
    elements[0] = element;

    HUD::GetInstance().SetElements(nPlayer, elements);

    switch(elements[0]+elements[1])
    {
        case 0:
            switch(elements[2])
            {
                case 0:
                    activeSkill = 0;
                    break;
                case 1:
                    activeSkill = 1;
                    break;
                case 2:
                    activeSkill = 2;
                    break;
            }
            break;
        case 1:
            activeSkill = 9;
            break;
        case 2:
            switch(elements[2])
            {
                case 0:
                    activeSkill = 3;
                    break;
                case 1:
                    activeSkill = 4;
                    break;
                case 2:
                    activeSkill = 5;
                    break;
            }
            break;
        case 4:
            switch(elements[2])
            {
                case 0:
                    activeSkill = 6;
                    break;
                case 1:
                    activeSkill = 7;
                    break;
                case 2:
                    activeSkill = 8;
                    break;
            }
            break;
    }
}


void Player::UseSkill()
{
    if(activeSkill<skillcount) {
    if(skills[activeSkill]->cdStamp<cd.Get()){    //Se ja deu o cooldown
        Boxe* b;
        Vec2 orig(box.GetCenter(), box.h, angle);   //Vetor com inicio no centro do player, modulo box.h e angulo angle

       if(skills[activeSkill]->Is("Projectile")){
            Game::GetInstance().GetCurrentState().AddObject(skills[activeSkill]->NewInstance(orig, angle, nPlayer));
        }else if(skills[activeSkill]->Is("BindSkill")){
             Game::GetInstance().GetCurrentState().AddObject(skills[activeSkill]->NewInstance(orig, angle, nPlayer));
        }else if(skills[activeSkill]->Is("BoxeSkill")){
            b=(Boxe*)(skills[activeSkill]->NewInstance(orig, angle, nPlayer));
            b->setSide(this->side);
            Game::GetInstance().GetCurrentState().AddObject(b);
        }else if (skills[activeSkill]->Is("Banana")){
            Vec2 trap(box.GetCenter(), -(box.h+10), angle);
            Game::GetInstance().GetCurrentState().AddObject_low(skills[activeSkill]->NewInstance(trap, angle, nPlayer));
        }
        else if(skills[activeSkill]->Is("Shield"))
        {
            Game::GetInstance().GetCurrentState().AddObject(skills[activeSkill]->NewInstance(orig, angle, nPlayer));
        }

        skills[activeSkill]->cdStamp = cd.Get()+skills[activeSkill]->cooldown;
        }
    }
}

