#include "../header/State.h"
#include "../header/Game.h"
#include "../header/InputManager.h"
#include "../header/Camera.h"
#include "../header/Collision.h"
#include <ctime>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <memory>

State::State(){
        State::quitRequested=false;
        State::popRequested=false;
}

State::~State(){
        objectArray.clear();
}

void State::AddObject(GameObject* ptr){

         objectArray.emplace_back(ptr);
}



void State::AddObject_low(GameObject* ptr){

         objectArray_low.emplace_back(ptr);
}


void State::AddObject_high(GameObject* ptr){

         objectArray_high.emplace_back(ptr);
}


bool State::QuitRequested(){return quitRequested;}
bool State::PopRequested(){return popRequested;}

void State::UpdateArray(float dt){}
void State::RenderArray(){}

void State::UpdateUI(float dt)
{
    int i;
    for(i = UIArray.size() - 1; i >= 0; i--){
        UIArray.at(i)->Update(Game::GetInstance().GetDeltaTime());
    }
}

void State::RenderUI()
{
    std::vector<std::unique_ptr<UI>>::iterator it;

    for(it = UIArray.begin(); it!=UIArray.end();it++){
        (*it)->Render();
    }
}

