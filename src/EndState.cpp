


#include "../header/EndState.h"
#include "../header/Text.h"
#include "../header/InputManager.h"
#include "../header/Game.h"
#include "../header/Camera.h"
#include "../header/TitleState.h"

    EndState::~EndState(){}

    EndState::EndState(StateData stateData){

        EndState::bg=*(new Sprite("img/hud/bb.png"));
        music = new Music();
        SDL_Color cor;
        cor.a=255;
        cor.r=255;
        cor.g=255;
        cor.b=255;
        std::string player;

        if(stateData.nPlayer==0) player="P1";
        else if(stateData.nPlayer==1) player="P2";
        else if (stateData.nPlayer==2) player="P3";
        else player="P4";

        instruction= new Text(FONT,36,SOLID,"Vencedor! "+player,cor,0,0);


        instruction->SetPos(Game::GetInstance().GetDisplay().x/2,100);
        time_texto = new Timer();
        mostrar_texto=true;

}

    void EndState::Update(){

        if(InputManager::GetInstance().KeyPress(SDLK_ESCAPE)){
            //Game::GetInstance().Push(new TitleState());
            popRequested=true;
            quitRequested=true;
        }
        if(InputManager::GetInstance().QuitRequested())
               quitRequested=true;


    if((time_texto->Get()>0.75 && mostrar_texto==true )|| (time_texto->Get()>0.45 && mostrar_texto==false )){
        time_texto->Restart();
        if(mostrar_texto)mostrar_texto=false;
            else mostrar_texto=true;
        }
    else
        time_texto->Update(Game::GetInstance().GetDeltaTime());

    }

    void EndState::Render(){


        bg.Render(0,0,0);
        if(mostrar_texto)
            instruction->Render(Camera::pos.x,Camera::pos.y);

    }


    void EndState::Pause(){}
    void EndState::Resume(){}
