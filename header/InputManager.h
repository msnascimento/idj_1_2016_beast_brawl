#ifndef HEADER_INPUT
#define HEADER_INPUT

#include <unordered_map>
#include <vector>
#include <memory>

#include <SDL2/SDL.h>
#define LEFT_ARROW_KEY SDLK_LEFT
#define RIGHT_ARROW_KEY SDLK_RIGHT
#define UP_ARROW_KEY SDLK_UP
#define DOWN_ARROW_KEY SDLK_DOWN
#define ESCAPE_KEY_BUTTON SDL_BUTTON_LEFT
#define LEFT_MOUSE_BUTTON SDL_BUTTON_LEFT
#define RIGHT_MOUSE_BUTTON SDL_BUTTON_RIGHT

#include "JoyStick.h"

class InputManager{
public:
    void Update();
    bool KeyPress(int key);
    bool KeyRelease(int key);
    bool IsKeyDown(int key);
    bool MousePress(int button);
    bool MouseRelease(int button);
    bool IsMouseDown(int button);
    int  GetMouseX();
    int  GetMouseY();
    bool QuitRequested();
    static InputManager& GetInstance();



    void SetJoy(int quantidade){nJoysticks=quantidade;}
    int GetJoy(){return nJoysticks;}
    void AddJoyStick(JoyStick* js);
    bool JoyPress(int joy,int key);
    bool JoyRelease(int joy,int key);
    bool IsJoyDown(int joy,int button);
    void ResetJoyStick();

    int GetAxisX(int joy);
    int GetAxisY(int joy);

private:
    InputManager();
    ~InputManager();
    bool mouseState[6];
    int mouseUpdate[6];
    std::unordered_map<int, bool> keyState;
    std::unordered_map<int, int> keyUpdate;
    bool quitRequested;
    int updateCounter;
    int mouseX;
    int mouseY;


    std::vector<std::unique_ptr<JoyStick>> JS;
    int nJoysticks;


};

#endif
