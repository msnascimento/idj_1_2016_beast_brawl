#include "../header/Banana.h"
#include "../header/Game.h"
#include "../header/Player.h"
#include "../header/Animation.h"
#include "../header/Camera.h"
#include "../header/Sound.h"

Banana::Banana(Vec2 orig,float angle, int source, string spname) : sp(spname)
{
    SetParameters(orig,BANANA_BASE_CD);
    cooldown = BASE_CD;

    collision = false;

    box.w = sp.GetWidth();
    box.h = sp.GetHeight();
    box.x = orig.x;
    box.y = orig.y;


  //  box.x = box.x * cos(angle) - box.y * sin(angle);
//    box.y = box.x * sin(angle) + box.y * cos(angle);

    box_projectile_colision.w = box.w;
    box_projectile_colision.h = box.h;
    box_projectile_colision.x = box.x;
    box_projectile_colision.y = box.y;

    //box.SetCenter(orig);
    Sound explosionse(BANANA_USE_SOUND);
    explosionse.Play(0);
}

Banana::~Banana() {
}

Banana::Banana()
{
    cdStamp = 0;
    cooldown = PROJECTILE_BASE_CD;
}

Skill* Banana::NewInstance(Vec2 orig, float angle, int source)
{
    return new Banana(orig, angle, source);
}

void Banana::Update(float dt) {
    /*box.x += speed.x*dt;
    box.y += speed.y*dt;
    box_projectile_colision.x += speed.x*dt;
    box_projectile_colision.y += speed.y*dt;
    sp.Update(dt);*/
}

void Banana::Render() {
    //sp.Render(box.x-Camera::pos.x - (sp.GetWidth()/2), box.y-Camera::pos.y - (sp.GetHeight()/2), angle*180/PI);
    sp.renderizar

}

bool Banana::IsDead() {
    if(box.x > 1280 || box.x < 0 || box.y > 720 || box.y < 0)    //Limites do mapa
    {
        return true;
    }
    else if(collision)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Banana::NotifyCollision(GameObject& other) {
    if(other.Is("Player") || other.Is("Objetos"))
    {
       // if(((Player&)other).nPlayer)//pode colidir com o proprio personagem
       // {
            collision = true;
            Animation *explosion = new Animation(box.x, box.y, 0, BANANA_SPRITE_EFFECT, 5, 0.5, true);
            Game::GetInstance().GetCurrentState().AddObject_high(explosion);
            Sound explosionse(BANANA_CONTACT_SOUND);
            explosionse.Play(0);
        //}
    }
}

bool Banana::Is(string type) {
    if(type == "Banana" || type == "Skill")
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Banana::SetBaseCD(float base_cd)
{
    this->BASE_CD = base_cd;
}


void Banana::SetParameters(Vec2 orig, float base_cd)
{
    this->pos=orig;
    this->BASE_CD = base_cd;
}

