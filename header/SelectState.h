
#ifndef HEADER_SSTATE
#define HEADER_SSTATE

#include <vector>
#include <memory>
#include "Music.h"
#include "State.h"
#include "Sprite.h"
#include "Text.h"
#include "Timer.h"
#include "GameObject.h"
#include "Sound.h"

class SelectState: public State{


public:
    SelectState();
    ~SelectState();

    void Update();
    void Render();
    void Pause();
    void Resume();

protected:
    void UpdateArray(float dt);
private:

    std::string personagens[4];
    int posicao[4];
    int posicao_off[4];
    Music* music;
    Sound* opcao;
    Sprite bg;
    Text* P1;
    Text* P2;
    Text* P3;
    Text* P4;
    Text* selecione;
    Text* start;
    Sprite ps1_sp;
    Sprite ps2_sp;
    Sprite ps3_sp;
    Sprite ps4_sp;
    Sprite ps1_off_sp;
    Sprite ps2_off_sp;
    Sprite ps3_off_sp;
    Sprite ps4_off_sp;
    Sprite player_off_sp;

    int contador1,contador2,contador3,contador4;
};
#endif

