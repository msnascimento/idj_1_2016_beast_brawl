
#include "../header/Resources.h"
#include "../header/Game.h"
#include "../header/Game.h"
#include<unordered_map>

#include<cstdlib>

std::unordered_map<std::string,SDL_Texture*> Resources::imageTable;
std::unordered_map<std::string,Mix_Music*> Resources::musicTable;
std::unordered_map<std::string,Mix_Chunk*> Resources::soundTable;
std::unordered_map<std::string,TTF_Font*>  Resources::fontTable;

     SDL_Texture* Resources::GetImage(std::string file){

    //imageTable.
    std::unordered_map<std::string,SDL_Texture*>::const_iterator got=Resources::imageTable.find(file);
        if (got!=Resources::imageTable.end())
            return got->second;
        else{
            SDL_Texture* text=IMG_LoadTexture(Game::GetInstance().GetRenderer(),file.data());
             if(text==nullptr){
                throw SDLException("8");
                }
            Resources::imageTable.emplace(file, text);
        return text;
        }
    }

     void Resources::ClearImages(){
        for ( auto it =  Resources::imageTable.begin(); it !=  Resources::imageTable.end(); ++it )
            SDL_DestroyTexture(it->second);

        Resources::imageTable.clear();

}


     Mix_Music* Resources::GetMusic(std::string file){

        std::unordered_map<std::string,Mix_Music*>::const_iterator got=Resources::musicTable.find(file);
        if (got!=Resources::musicTable.end())
            return got->second;
        else{
            Mix_Music* m = Mix_LoadMUS(file.data());

            if(m == nullptr) {
                printf("Mix_Init: %s\n", Mix_GetError());
                throw SDLException("9");
            }
            Resources::musicTable.emplace(file, m);
            return m;
        }

    }



     void Resources::ClearMusic(){
        for ( auto it =  Resources::musicTable.begin(); it !=  Resources::musicTable.end(); ++it )
            Mix_FreeMusic(it->second);

        Resources::musicTable.clear();

}


     Mix_Chunk* Resources::GetSound(std::string file){

        std::unordered_map<std::string,Mix_Chunk*>::const_iterator got=Resources::soundTable.find(file);
        if (got!=Resources::soundTable.end())
            return got->second;
        else{
                Mix_Chunk* m = Mix_LoadWAV(file.data());
            if(m==nullptr){
                throw SDLException("10");
            }
            Resources::soundTable.emplace(file, m);
            return m;
        }
    }



     void Resources::ClearSound(){
        for ( auto it =  Resources::soundTable.begin(); it !=  Resources::soundTable.end(); ++it )
            Mix_FreeChunk(it->second);

        Resources::soundTable.clear();

}



     TTF_Font* Resources::GetFont(std::string file, int fontSize){


        std::string string_concate=file;
        string_concate+= (char)fontSize;


        std::unordered_map<std::string,TTF_Font*>::const_iterator got=Resources::fontTable.find(string_concate);
        if (got!=Resources::fontTable.end())
            return got->second;
        else{
            TTF_Font* f = TTF_OpenFont (file.data(),fontSize);
             if(f==nullptr){
                throw SDLException("11");
            }
            //concatenar
            Resources::fontTable.emplace(string_concate, f);
            return f;
        }
    }



     void Resources::ClearFont(){
        for ( auto it =  Resources::fontTable.begin(); it !=  Resources::fontTable.end(); ++it )
            TTF_CloseFont(it->second);
        Resources::fontTable.clear();

}

