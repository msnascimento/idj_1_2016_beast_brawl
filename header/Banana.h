
#ifndef HEADER_BANANA
#define	HEADER_BANANA

#include <string>
#include "Skill.h"
#include "Sprite.h"
#include "Vec2.h"

#define BANANA_BASE_DMG 0  //dano
#define BANANA_SPEEDMOD 0  //velocidade do tiro
#define BANANA_BASE_CD  10   //Cooldown em segundos
#define BANANA_SPRITE "img/skills/banana.png"    //caminho da imagem do sprite
#define BANANA_SPRITE_EFFECT "img/skills/banana.png"    //caminho da imagem do sprite
#define BANANA_CONTACT_SPRITE "img/explosion.png"   //caminho do sprite quando o projetil colide
#define BANANA_USE_SOUND "audio/Sons/WAV/SetTrap.wav"       //caminho do som quando o projetil colide
#define BANANA_CONTACT_SOUND "audio/Sons/WAV/GetTrap.wav"       //caminho do som quando o projetil colide



using namespace std;

class Banana: public Skill{
public:
    Banana(Vec2, float angle,int source,  string spname = BANANA_SPRITE);
    Banana();   //fabrica de bananas
    virtual ~Banana();
    void Update(float dt);
    void Render();
    bool IsDead();
    virtual void NotifyCollision (GameObject& other);
    virtual bool Is (string type);

    virtual Skill* NewInstance(Vec2 orig, float angle, int source);
    void SetParameters(Vec2 orig,float base_cd);

    void SetBaseCD(float base_cd);

protected:
    bool collision;
private:
    Sprite sp;
    Vec2 pos;
    float BASE_CD;    //Cooldown base

};

#endif	/* HEADER_BANANA */

