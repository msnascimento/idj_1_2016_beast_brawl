#ifndef HEADER_GAME
#define HEADER_GAME

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include "State.h"
#include<stack>


class Game{

public:
    Game(std::string title,int width,int height);
    ~Game();
    void    Run();

    SDL_Renderer* GetRenderer();
    static Game& GetInstance();
    float GetDeltaTime();

    void Push(State* state);
    State& GetCurrentState();
    Vec2 GetDisplay();
private:
    static Game* instance;
    Vec2 display;
    State* storedState;
    SDL_Window* window;
    SDL_Renderer* renderer;
    std::stack<std::unique_ptr<State>> stateStack;
    int frameStart;
    float dt;

    void CalculateDeltaTime();
};

#endif
