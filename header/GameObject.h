#ifndef HEADER_GAMEO
#define HEADER_GAMEO
#include "Rect.h"

#include <string>

class GameObject{
public:

 GameObject(){rotation=0;}
virtual ~GameObject(){}
virtual void Update(float dt)=0;
virtual void Render()=0;
virtual bool IsDead()=0;
virtual void NotifyCollision (GameObject& other) =0;
virtual bool Is (std::string type) =0;
Rect box;
Rect box_projectile_colision;
float rotation;

};
#endif
