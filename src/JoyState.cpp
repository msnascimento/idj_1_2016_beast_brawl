

#include "../header/JoyState.h"
#include "../header/InputManager.h"
#include "../header/Game.h"
#include "../header/Camera.h"

#include <string>


#define escala 0.8
#define FONT "font/Call me maybe.ttf"
#define FONT "font/werebeast.ttf"

    JoyState::JoyState(){
        JoyState::bg= *(new Sprite("img/hud/menuescalado.png"));
        bg.SetClip(0,0,1280,720);

        JoyState::sp =*(new Sprite("img/joystick/joypad.png"));
        sp.SetClip(0,0,500,400);
        sp.SetScaleX(escala);
        sp.SetScaleY(escala);
        std::string st;
         Sprite button;
        for(int i=0; i<MAX_BUTTONS;i++){
                st="img/joystick/joypad";
                if(i<10){
                    st+=(char)(48+i);
                }else{
                    st+=(char)(48+i/10);
                    st+=(char)(48+i%10);
                }
                st+=".png";

                if(i!= 8 && i!= 9 && i!= 10 && i!=11)
                    button= *(new Sprite(st));
                else
                   button= *(new Sprite("img/joystick/joypad.png"));

                button.SetScaleX(escala);
                button.SetScaleY(escala);
                buttons.emplace_back(button);
        }




        SDL_Color cor;
        cor.a=255;
        cor.r=255;
        cor.g=0;
        cor.b=0;


        R1= new Text(FONT,30,SOLID,"R1",cor,425*escala,0);
        R2= new Text(FONT,30,SOLID,"R2",cor,345*escala,0);
        L1= new Text(FONT,30,SOLID,"L1",cor,60*escala,0);
        L2= new Text(FONT,30,SOLID,"L2",cor,130*escala,0);

        for(int j=0;j<4;j++)
            for(int i=0;i<MAX_BUTTONS;i++)
                press[j][i]=false;


        cor.r=200;cor.g=0;cor.b=0;
        P1= new Text(FONT,50,SOLID,"P1",cor,520*escala,100*escala);
        cor.r=0;cor.g=200;cor.b=0;
        P2= new Text(FONT,50,SOLID,"P2",cor,1025*escala,100*escala);
        cor.r=0;cor.g=0;cor.b=200;
        P3= new Text(FONT,50,SOLID,"P3",cor,520*escala,600*escala);
        cor.r=200;cor.g=200;cor.b=0;
        P4= new Text(FONT,50,SOLID,"P4",cor,1025*escala,600*escala);

        cor.r=0;cor.g=0;cor.b=0;
        Atualizar= new Text(FONT,36,SOLID,"Atualizar JS's",cor,0,0);
        Voltar= new Text(FONT,36,SOLID,"Voltar",cor,0,0);

        Atualizar->SetPos(Game::GetInstance().GetDisplay().x/2,Game::GetInstance().GetDisplay().y/4);
        Voltar->SetPos(Game::GetInstance().GetDisplay().x/2,Game::GetInstance().GetDisplay().y/1.4);

        opcao= new Sound();
        opcao->Open("audio/Sons/WAV/Select1.wav");
    }


    void JoyState::Update(){
        SDL_Color cor;
        cor.a=255;cor.r=255;cor.g=255;cor.b=255;
        SDL_Color cor2;
        cor2.a=255;cor2.r=150;cor2.g=0;cor2.b=0;


        if(InputManager::GetInstance().KeyPress(SDLK_ESCAPE)){
               //State::quitRequested=true;
               this->popRequested=true;
               }
        if(InputManager::GetInstance().QuitRequested())
               State::quitRequested=true;


        for(int j=0; j<InputManager::GetInstance().GetJoy();j++){
            for(int i=0; i<MAX_BUTTONS;i++){
                if(InputManager::GetInstance().IsJoyDown(j,i))
                    press[j][i]=true;
                else
                    press[j][i]=false;
            }
        }



        //atualizar joysticks
        if(Atualizar->IsMouse_TextInside()){
            Atualizar->SetColor(cor);

            if(InputManager::GetInstance().MousePress(LEFT_MOUSE_BUTTON)){

                InputManager::GetInstance().ResetJoyStick();
                InputManager::GetInstance().SetJoy(0);

                for(int j=0;j<4;j++)
                    for(int i=0;i<MAX_BUTTONS;i++)
                        press[j][i]=false;

                if(SDL_NumJoysticks()>0){
                    // inicializa joysticks
                    SDL_JoystickEventState(SDL_ENABLE);
                    printf("%i joysticks:\n", SDL_NumJoysticks() );

                    InputManager::GetInstance().SetJoy(SDL_NumJoysticks());

                    for(int i=0; i < InputManager::GetInstance().GetJoy(); i++ ){
                            SDL_Joystick *joystick = SDL_JoystickOpen(i);
                            InputManager::GetInstance().AddJoyStick(new JoyStick(joystick));
                            printf("%s\n", SDL_JoystickName(joystick));
                    }
                    printf("\n");
                }else{
                    //sem joysticks
                         printf("Nenhum joystick encontrado\n\n");

                    }
                    opcao->Play(1);
            }

        }else{

            Atualizar->SetColor(cor2);

        }

        if(Voltar->IsMouse_TextInside()){
            Voltar->SetColor(cor);
            if(InputManager::GetInstance().MousePress(LEFT_MOUSE_BUTTON)){
                this->popRequested=true;
                opcao->Play(1);
            }
        }else{
            Voltar->SetColor(cor2);

        }


    }


    void JoyState::Render(){


        //renderizar o fundo
        bg.Render(0,0);
        //renderizar texto
        Atualizar->Render();
        Voltar->Render();
        //renderizar os controles conectados
       switch(InputManager::GetInstance().GetJoy()){
            case 4:
                sp.Render(Game::GetInstance().GetDisplay().x-sp.GetWidth(),Game::GetInstance().GetDisplay().y-sp.GetHeight());
                P4->Render();
            case 3:
                sp.Render(0,Game::GetInstance().GetDisplay().y-sp.GetHeight());
                P3->Render();
            case 2:
               sp.Render(Game::GetInstance().GetDisplay().x-sp.GetWidth(),0);
               P2->Render();
            case 1:
                sp.Render(0,0);
                P1->Render();
            case 0:
            break;

        }



        //renderizar os botoes precionados
        for(int j=0; j<InputManager::GetInstance().GetJoy();j++){
            for(int i=0; i<MAX_BUTTONS;i++){
                if(i!=8 && i!=9 && i!=10 && i!=11){
                    if(press[j][i]){
                        if(j==0)
                            buttons.at(i).Render(0,0);
                        if(j==1)
                            buttons.at(i).Render(Game::GetInstance().GetDisplay().x-sp.GetWidth(),0);
                        if(j==2)
                            buttons.at(i).Render(0,Game::GetInstance().GetDisplay().y-sp.GetHeight());
                        if(j==3)
                            buttons.at(i).Render(Game::GetInstance().GetDisplay().x-sp.GetWidth(),Game::GetInstance().GetDisplay().y-sp.GetHeight());
                    }
                }else if(i==8){
                    if(press[j][i]){
                        if(j==0)
                            JoyState::L2->Render(0,0);
                        if(j==1)
                            JoyState::L2->Render(Game::GetInstance().GetDisplay().x-sp.GetWidth(),0);
                        if(j==2)
                            JoyState::L2->Render(0,Game::GetInstance().GetDisplay().y-sp.GetHeight());
                        if(j==3)
                            JoyState::L2->Render(Game::GetInstance().GetDisplay().x-sp.GetWidth(),Game::GetInstance().GetDisplay().y-sp.GetHeight());
                    }
                }else if(i==9){
                    if(press[j][i]){
                        if(j==0)
                            JoyState::R2->Render(0,0);
                        if(j==1)
                            JoyState::R2->Render(Game::GetInstance().GetDisplay().x-sp.GetWidth(),0);
                        if(j==2)
                            JoyState::R2->Render(0,Game::GetInstance().GetDisplay().y-sp.GetHeight());
                        if(j==3)
                            JoyState::R2->Render(Game::GetInstance().GetDisplay().x-sp.GetWidth(),Game::GetInstance().GetDisplay().y-sp.GetHeight());
                    }
                }else if(i==10){
                   if(press[j][i]){
                        if(j==0)
                            JoyState::L1->Render(0,0);
                        if(j==1)
                            JoyState::L1->Render(Game::GetInstance().GetDisplay().x-sp.GetWidth(),0);
                        if(j==2)
                            JoyState::L1->Render(0,Game::GetInstance().GetDisplay().y-sp.GetHeight());
                        if(j==3)
                            JoyState::L1->Render(Game::GetInstance().GetDisplay().x-sp.GetWidth(),Game::GetInstance().GetDisplay().y-sp.GetHeight());
                     }
                }else if(i==11){
                    if(press[j][i]){ if(j==0)
                            JoyState::R1->Render(0,0);
                        if(j==1)
                            JoyState::R1->Render(Game::GetInstance().GetDisplay().x-sp.GetWidth(),0);
                        if(j==2)
                            JoyState::R1->Render(0,Game::GetInstance().GetDisplay().y-sp.GetHeight());
                        if(j==3)
                            JoyState::R1->Render(Game::GetInstance().GetDisplay().x-sp.GetWidth(),Game::GetInstance().GetDisplay().y-sp.GetHeight());
                    }
                }
            }

        }

    }


    void JoyState::Pause(){

    }

    void JoyState::Resume(){

    }


