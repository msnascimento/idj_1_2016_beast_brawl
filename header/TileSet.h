#ifndef HEADER_TSET
#define HEADER_TSET

#include "Sprite.h"
class TileSet{

public:
  TileSet(int tileWidth, int tileHeight,std::string file, float fator=1);

  void Render(unsigned int index,float x, float y);

  int GetTileWidth();
  int GetTileHeight();

private:
  Sprite tileSet;
  unsigned int rows;
  unsigned int columns;
  int tileWidth;
  int tileHeight;

};

#endif
