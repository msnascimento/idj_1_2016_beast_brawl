

#ifndef HEADER_ANIM
#define HEADER_ANIM

#include "Vec2.h"
#include "GameObject.h"
#include "Sprite.h"
#include "Timer.h"
#include <string>
class Animation: public GameObject{


public:

    Animation(int x,
                int y,
                float rotation,
                std::string sprite,
                float timeLimit,
                bool ends);

    Animation(int x,
                int y,
                float rotation,
                std::string sprite,
                int frameCount,
                float frameTime,
                bool ends);

    void Update(float dt);
    void Render();
    bool IsDead();
    void NotifyCollision(GameObject& other);
    bool Is(std::string type);
    int alienCount;

private:

    Timer endTimer;
    float timeLimit;
    bool oneTimeOnly;
    Sprite sp;
    enum AlienState { MOVING, RESTING };
    AlienState state;
    Timer restTimer;
    Vec2 destination;


};
#endif

