#include "../header/HUD.h"

HUD* HUD::instance=nullptr;

HUD::HUD(): UI(-1, nullptr)
{
/*TODO: HUD eh a mainframe (id=-1) e estatico (acessivel a player (igual a camera)
        Metodos estaticos para atualizar hp e elementos, a serem usados pelos personagens
        Metodos comuns para implementar os virtuais puros de UI(update e render) a serem usados pela fighstage
    Cada personagem tem um quadro no mainframe.
    Cada quadro tem o hp, imagem do personagem, nome e elementos ativos(?)

    -Menor prioridade: alguma arte entre os quadros
*/

    HUD::instance=this;

    //Background do HUD
    sp.Open(HUDBG);
    box.w = sp.GetWidth();
    box.h = sp.GetHeight();
    sp.SetClip(0, 0, box.w, box.h);
    box.x = box.y = 0;
}

HUD::~HUD()
{
    //dtor
}

HUD& HUD::GetInstance(){
    return *instance;
}

void HUD::Update(float dt)
{
    UpdateInner(dt);
}

void HUD::Render()
{
    sp.Render(box.x, box.y);
    RenderInner();
}

void HUD::AddCard(int joynum, Snap snap, string img)
{
    AddInnerUI(new PlayerCard(joynum, snap, img, this));
}

void HUD::SetHP(int joynum, float value)
{
    for (auto& it : innerArray)
    {
        if(it.second->id == joynum)
        {
            ((PlayerCard*)it.second.get())->SetHP(value);
        }
    }
}

void HUD::SetElements(int joynum, int elements[3])
{
    for (auto& it : innerArray)
    {
        if(it.second->id == joynum)
        {
            ((PlayerCard*)it.second.get())->SetElements(elements);
        }
    }
}
