#ifndef HEADER_RECT
#define HEADER_RECT

#include "Vec2.h"
class Rect{


private:


public:
     float w,h;
     float x,y; //top left
    Vec2* centro=new Vec2(0.0,0.0);
    bool IsInside(float xx, float yy);
    //center
    void SetRect(float xx, float yy, float ww, float hh);

    Vec2 GetCenter() const;
    void SetCenter(Vec2 center);
    void setX(float xx);
    void setY(float yy);
    void setW(float ww);
    void setH(float hh);


    //center
    float getX();
    float getY();
    float getW();
    float getH();

};
#endif

