
#include "../header/Rect.h"

   bool Rect::IsInside(float xx, float yy){

        if(xx-x>=0 && xx-x<=w && yy-y>=0 && yy-y<=h)
            return true;
        else
            return false;
    }

    //center
    void Rect::SetRect(float xx, float yy, float ww, float hh){
        x=xx;
        y=yy;
        w=ww;
        h=hh;
        centro->x=xx;
        centro->y=yy;
    }


    void Rect::setX(float xx){
        x=xx;
        centro->x=xx;
    }
    void Rect::setY(float yy){
        y=yy;
        centro->y=yy;
    }
    void Rect::setW(float ww){
        w=ww;
    }
    void Rect::setH(float hh){
        h=hh;
    }



    //center
    float Rect::getX(){
        return x;
    }
    float Rect::getY(){
        return y;
    }

    float Rect::getW(){
        return w;
    }
    float Rect::getH(){
        return h;
    }


    Vec2 Rect::GetCenter() const{
        return *centro;
    }

    void Rect::SetCenter(Vec2 center)
    {
        this->x = center.x- (w/2);
        this->y = center.y - (h/2);
    }

