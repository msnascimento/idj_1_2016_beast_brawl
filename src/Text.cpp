#include "../header/Text.h"
#include "../header/Resources.h"
#include "../header/Game.h"
#include "../header/InputManager.h"

    Text::Text (std::string fontFile, int fontSize, TextStyle style, std::string text, SDL_Color color, int x, int y){

        font=Resources::GetFont(fontFile,fontSize);
        texture=nullptr;

        this->text=text;
        this->style=style;
        this->fontSize=fontSize;
        this->color=color;
        this->box.setX(x);
        this->box.setY(y);
        RemakeTexture();
    }


    Text::~Text(){
        if(texture!=nullptr)
            SDL_DestroyTexture(texture);
    }

    void Text::Render(int cameraX , int cameraY){
        SDL_Rect destino;
        destino.h=box.h;
        destino.w=box.w;
        destino.x=box.getX()+cameraX;
        destino.y=box.getY()+cameraY;


        SDL_Rect clipRect;
        clipRect.h=box.h;
        clipRect.w=box.w;
        clipRect.x=0;
        clipRect.y=0;


        if (texture!=nullptr)
            SDL_RenderCopy(Game::GetInstance().GetRenderer(),texture,&clipRect,&destino);

    }

    void Text::SetPos(int x , int y,bool centerX , bool centerY ){


        if(centerX){
            box.setX(x);
        }else
            box.setX(x-box.getW()/2);

        if(centerY){
            box.setY(y);
        }else
            box.setY(y-box.getH()/2);

        RemakeTexture();
    }

    void Text::SetText(std::string text){

        this->text=text;
        RemakeTexture();
    }

    void Text::SetColor(SDL_Color color){

        this->color=color;
        RemakeTexture();
    }

    void Text::SetStyle(TextStyle style){

        this->style=style;
        RemakeTexture();
    }

    void Text::SetFontSize(int fontSize){

        this->fontSize=fontSize;
        RemakeTexture();
    }

    void Text::RemakeTexture(){

        SDL_Surface* surface;
        if (texture!=nullptr)
            SDL_DestroyTexture(texture);

        if(style==SOLID)
            surface= TTF_RenderText_Solid(font, text.data(), color);
        else if(style==SHADED)
            surface= TTF_RenderText_Shaded(font, text.data(), color,color);
        else
            surface= TTF_RenderText_Blended(font, text.data(), color);

        box.setH(surface->clip_rect.h);
        box.setW(surface->clip_rect.w);

        texture=SDL_CreateTextureFromSurface(Game::GetInstance().GetRenderer(),surface);

        SDL_FreeSurface(surface);
    }



bool Text::IsMouse_TextInside(){

       if( this->box.x<InputManager::GetInstance().GetMouseX() &&
        this->box.x+this->box.w>InputManager::GetInstance().GetMouseX() &&
        this->box.y+10<InputManager::GetInstance().GetMouseY() &&
        this->box.y+this->box.h-5>InputManager::GetInstance().GetMouseY())
    return true;
    else return false;
}






