
#ifndef HEADER_FSTATE
#define HEADER_FSTATE

#include <vector>
#include <memory>
#include "Music.h"
#include "State.h"
#include "Sprite.h"
#include "TileSet.h"
#include "TileMap.h"
#include "Text.h"
#include "Timer.h"
#include "GameObject.h"
#include "HUD.h"

class FightState: public State{


public:
    FightState(std::string  personagens[]);
    ~FightState();

    void Update();
    void Render();
    void Pause();
    void Resume();

protected:
    void UpdateArray(float dt);
    void RenderArray();
private:
    void Layer2Obj(int layer,std::vector<int> mapa);
    std::vector<int> Gerar_Dominos(std::vector<int> mapa);
    std::vector<int> Gerar_Legos(std::vector<int> mapa);
    std::vector<int> Gerar_Cartas(std::vector<int> mapa);
    Music* music;
    Sprite bg;
    TileSet* tileSet;
    TileMap* tileMap;
    HUD hud;

    Timer playerdeath;
    bool morreu;
};
#endif
