#include "../header/Projectile.h"
#include "../header/Game.h"
#include "../header/Player.h"
#include "../header/Animation.h"
#include "../header/Camera.h"
#include "../header/Sound.h"

Projectile::Projectile(Vec2 orig, float angle, int source, string spname) : sp(spname, 4, 0.2)
{
    Sound explosionse(PROJECTILE_LAUNCH_SOUND);
    explosionse.Play(0);

    SetParameters(PROJECTILE_SPEEDMOD, PROJECTILE_BASE_CD, PROJECTILE_BASE_DMG);
    speed.x = speedmod;
    speed.y = 0;
    cooldown = BASE_CD;

    //angle
    this->angle = angle;
    //rotation = angle;std::cout<<angle<<endl;

    this->source = source;
    speed.Rot(angle);
    collision = false;

    box.w = sp.GetWidth();
    box.h = sp.GetHeight();
    box.x = orig.x;
    box.y = orig.y;

  //  box.x = box.x * cos(angle) - box.y * sin(angle);
//    box.y = box.x * sin(angle) + box.y * cos(angle);

    if(angle != 0 && (angle>PI+0.01 || angle<PI-0.01))
    {
        float temp;
        temp = box.w;
        box.w = box.h;
        box.h = temp;
    }

    box_projectile_colision.w = box.w;
    box_projectile_colision.h = box.h;
    box_projectile_colision.x = box.x;
    box_projectile_colision.y = box.y;

    //box.SetCenter(orig);
}

Projectile::~Projectile() {
}

Projectile::Projectile()
{
    cdStamp = 0;
    cooldown = PROJECTILE_BASE_CD;
}

Skill* Projectile::NewInstance(Vec2 orig, float angle, int source)
{
    return new Projectile(orig, angle, source);
}

void Projectile::Update(float dt) {
    box.x += speed.x*dt;
    box.y += speed.y*dt;
    box_projectile_colision.x += speed.x*dt;
    box_projectile_colision.y += speed.y*dt;
    sp.Update(dt);
}

void Projectile::Render() {
    sp.Render(box.x-Camera::pos.x - (sp.GetWidth()/2), box.y-Camera::pos.y - (sp.GetHeight()/2), angle*180/PI);
}

bool Projectile::IsDead() {
    if(box.x > 1280 || box.x < 0 || box.y > 720 || box.y < 0)    //Limites do mapa
    {
        return true;
    }
    else if(collision)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Projectile::NotifyCollision(GameObject& other) {
    if(other.Is("Player") || other.Is("Objetos"))
    {
        if(((Player&)other).nPlayer != source)//nao deve colidir com o proprio atirador
        {
            collision = true;
            Animation *explosion = new Animation(box.x, box.y, 0, PROJECTILE_CONTACT_SPRITE, 11, 0.2, true);
            Game::GetInstance().GetCurrentState().AddObject(explosion);
            Sound explosionse(PROJECTILE_CONTACT_SOUND);
            explosionse.Play(0);
        }
    }
    if(other.Is("Shield"))
    {
        collision = true;
        Sound explosionse(SHIELD_CONTACT_SOUND);
        explosionse.Play(0);
    }
}

bool Projectile::Is(string type) {
    if(type == "Projectile" || type == "Skill")
    {
        return true;
    }
    else
    {
        return false;
    }
}
void Projectile::SetSpeed(float speedmod)
{
    this->speedmod = speedmod;
}

void Projectile::SetBaseCD(float base_cd)
{
    this->BASE_CD = base_cd;
}

void Projectile::SetDmg(int dmg)
{
    this->dmg = dmg;
}

void Projectile::SetParameters(float speedmod, float base_cd, int dmg)
{
    this->speedmod = speedmod;
    this->BASE_CD = base_cd;
    this->dmg = dmg;
}
