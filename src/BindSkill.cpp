#include "../header/BindSkill.h"
#include "../header/Player.h"
#include "../header/Animation.h"
#include "../header/Game.h"
#include "../header/Sound.h"

BindSkill::BindSkill(Vec2 orig, float angle, int source) : Projectile(orig, angle, source, BIND_SPRITE)
{
    sp.SetFrameTime(0.1);
    SetParameters(BIND_SPEEDMOD, BIND_BASE_CD, BIND_BASE_DMG);
    box.w = box.w-20;
    box.h=box.h-20;
}

BindSkill::BindSkill()
{
    cdStamp = 0;
    cooldown = BIND_BASE_CD;
}

BindSkill::~BindSkill()
{
    //dtor
}

void BindSkill::NotifyCollision(GameObject& other)
{
    if(other.Is("Player") || other.Is("Objetos"))
    {
        if(((Player&)other).nPlayer != source)//nao deve colidir com o proprio atirador
        {
            collision = true;
            Animation *explosion = new Animation(box.x, box.y, 0, BIND_CONTACT_SPRITE, 4, 0.2, true);
            Game::GetInstance().GetCurrentState().AddObject(explosion);
            Sound explosionse(BIND_CONTACT_SOUND);
            explosionse.Play(0);
        }
    }
    if(other.Is("Shield"))
    {
        collision = true;
        Sound explosionse(SHIELD_CONTACT_SOUND);
        explosionse.Play(0);
    }
}

bool BindSkill::Is(string type)
{
    if(type == "BindSkill" || type == "Skill")
    {
        return true;
    }
    else
    {
        return false;
    }
}

Skill* BindSkill::NewInstance(Vec2 orig, float angle, int source)
{
    return new BindSkill(orig, angle, source);
}
