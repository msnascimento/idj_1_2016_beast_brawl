#ifndef BOXESKILL_H
#define BOXESKILL_H

#include "../header/Skill.h"
#include "../header/Projectile.h"

#define BOXE_BASE_DMG 20
#define BOXE_SPEEDMOD 45
#define BOXE_BASE_CD 1.4
#define BOXE_SPRITE "img/skills/boxe.png"
#define BOXE_CONTACT_SPRITE "img/efeitos/explosao2.png"
#define BOXE_CONTACT_SOUND "audio/Sons/WAV/boxe.wav"
#define BACK_DURATION 0.8

//Skill que prende
//Player atira um projetil(mesma logica de projectile) que prende quando em contato com inimigo
//A logica da skill esta basicamente no IsColliding do player
class Boxe : public Projectile
{
    public:
        Boxe(Vec2 orig, float angle, int source);
        Boxe();
        virtual ~Boxe();
        void NotifyCollision (GameObject& other);
        bool Is (string type);
        Skill* NewInstance(Vec2 orig, float angle, int source);
        void setSide(int s){side=s;}
        int getSide(){return side;}
    protected:
    private:
    int side;
};

#endif // BINDSKILL_H

