#include "../header/Boxe.h"
#include "../header/Player.h"
#include "../header/Animation.h"
#include "../header/Game.h"
#include "../header/Sound.h"

Boxe::Boxe(Vec2 orig, float angle, int source) : Projectile(orig, angle, source, BOXE_SPRITE)
{
    sp.SetFrameTime(0.1);
    SetParameters(BOXE_SPEEDMOD, BOXE_BASE_CD, BOXE_BASE_DMG);
    box.w = box.w-10;
    box.h=box.h-10;

}

Boxe::Boxe()
{
    cdStamp = 0;
    cooldown = BOXE_BASE_CD;
}

Boxe::~Boxe()
{
    //dtor
}

void Boxe::NotifyCollision(GameObject& other)
{
    if(other.Is("Player") || other.Is("Objetos")) //  || escudo?
    {
        if(((Player&)other).nPlayer != source)//nao deve colidir com o proprio atirador
        {
            collision = true;
            Animation *explosion = new Animation(box.x, box.y, 0, BOXE_CONTACT_SPRITE, 5, 0.15, true);
            Game::GetInstance().GetCurrentState().AddObject(explosion);
            Sound explosionse(BOXE_CONTACT_SOUND);
            explosionse.Play(0);
        }
    }
    if(other.Is("Shield"))
    {
        collision = true;
        Sound explosionse(SHIELD_CONTACT_SOUND);
        explosionse.Play(0);
    }
}

bool Boxe::Is(string type)
{
    if(type == "BoxeSkill" || type == "Skill")
    {
        return true;
    }
    else
    {
        return false;
    }
}

Skill* Boxe::NewInstance(Vec2 orig, float angle, int source)
{
    return new Boxe(orig, angle, source);
}

