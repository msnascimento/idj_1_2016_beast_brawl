
#ifndef HEADER_TSTATE
#define HEADER_TSTATE

#include "Music.h"
#include "State.h"
#include "Sprite.h"
#include "Text.h"
#include "Timer.h"
#include "Sound.h"

#define FONT "font/Call me maybe.ttf"
#define FONT "font/werebeast.ttf"

class TitleState: public State{


public:
    TitleState();


    void Update();
    void Render();
    void Pause();
    void Resume();

private:

    Music* music;
    Sound* opcao;
    Text* iniciar;
    Text* sair;
    Text* input;

    Sprite bg;
    Timer* time_texto;
    bool mostrar_texto;
};

#endif
