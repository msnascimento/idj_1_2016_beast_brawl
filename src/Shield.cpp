#include "../header/Shield.h"

Shield::Shield()
{
    cdStamp = 0;
    cooldown = SHIELD_BASE_CD;
}

Shield::~Shield()
{
    //dtor
}

Shield::Shield(Vec2 orig, float angle, int source, string spname) : sp(spname, 4, 0.1)
{
    Sound explosionse(SHIELD_USE_SOUND);
    explosionse.Play(0);

    duration.Restart();
    cooldown = SHIELD_BASE_CD;

    this->source = source;

    box.w = sp.GetWidth();
    box.h = sp.GetHeight();
    box.x = orig.x;
    box.y = orig.y;
    box_projectile_colision.w = box.w;
    box_projectile_colision.h = box.h;
    box_projectile_colision.x = box.x;
    box_projectile_colision.y = box.y;
}

void Shield::Update(float dt)
{
    duration.Update(dt);
    sp.Update(dt);
}

void Shield::Render()
{
    sp.Render(box.x - (sp.GetWidth()/2), box.y - (sp.GetHeight()/2));
}

bool Shield::IsDead()
{
    if(duration.Get()>SHIELD_DURATION)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Shield::NotifyCollision(GameObject& other)
{
    //Shield n colide com nada (mas skills de outros players colidem com o shield)
}

bool Shield::Is(string type)
{
    if(type == "Shield" || type == "Skill")
    {
        return true;
    }
    else
    {
        return false;
    }
}

Skill* Shield::NewInstance(Vec2 orig, float angle, int source)
{
    return new Shield(orig, angle, source);
}
