#ifndef SHIELD_H
#define SHIELD_H

#include "../header/Skill.h"
#include "../header/Sprite.h"
#include "../header/Sound.h"
#define SHIELD_SPRITE "img/skills/shield.png"
#define SHIELD_USE_SOUND "audio/Sons/WAV/Projetil1.wav"
#define SHIELD_DURATION 2
#define SHIELD_BASE_CD 5

class Shield : public Skill
{
    public:
        Shield();
        Shield(Vec2 orig, float angle, int source, string spname = SHIELD_SPRITE);
        virtual ~Shield();
        void Update(float dt);
        void Render();
        bool IsDead();
        virtual void NotifyCollision (GameObject& other);
        virtual bool Is (string type);

        virtual Skill* NewInstance(Vec2 orig, float angle, int source);
        int dmg;
        int source;

    private:
        Sprite sp;
        Timer duration;
        float speedmod;
        float BASE_CD;    //Cooldown base


};

#endif // SHIELD_H
