#ifndef HEADER_R
#define HEADER_R

#include<unordered_map>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

class Resources{
public:
    static SDL_Texture* GetImage(std::string file);
    static Mix_Music* GetMusic (std::string file);
    static Mix_Chunk* GetSound (std::string file);
    static TTF_Font* GetFont (std::string file, int fontSize);
    static void ClearImages();
    static void ClearMusic();
    static void ClearSound();
    static void ClearFont();
private:
    static std::unordered_map<std::string,SDL_Texture*> imageTable;
    static std::unordered_map<std::string,Mix_Music*> musicTable;
    static std::unordered_map<std::string,Mix_Chunk*> soundTable;
    static std::unordered_map<std::string,TTF_Font*> fontTable;
};
#endif
