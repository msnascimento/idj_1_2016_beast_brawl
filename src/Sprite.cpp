#include "../header/Sprite.h"
#include "../header/Game.h"
#include "../header/Resources.h"


Sprite::~Sprite(){}


Sprite::Sprite(){
        texture=nullptr;
        frameCount = frameTime = 1;
        currentFrame = 0;
        timeElapsed = 0;
        scaleX=1;
        scaleY=1;
        SetFrame(0);
    }

Sprite::Sprite(std::string file ,int Count ,float Time){
    texture=nullptr;
    Open(file);
    Sprite::frameCount=Count;
    currentFrame=0;
    timeElapsed=0;
    frameTime=Time;
    scaleX=1;
    scaleY=1;
    this->SetFrame(0);
    this->file=file;
}


void Sprite::Open(std::string file){
        texture=Resources::GetImage(file);
        SDL_QueryTexture(texture,nullptr,nullptr,&width,&height);
}


    void Sprite::Render(int x, int y, float angle){
            SDL_Rect destino;
            destino.h=clipRect.h*scaleY;
            destino.w=clipRect.w*scaleX;
            destino.x=x;
            destino.y=y;
            if (angle==0)
                SDL_RenderCopy(Game::GetInstance().GetRenderer(),texture,&clipRect,&destino);
            else
                SDL_RenderCopyEx(Game::GetInstance().GetRenderer(),texture,&clipRect,&destino,angle,nullptr,(SDL_FLIP_NONE));

    }


    void Sprite::SetClip(int x,int y, int w, int h){
            clipRect.h=h;
            clipRect.w=w;
            clipRect.x=x;
            clipRect.y=y;
    }



    int Sprite::GetWidth(){
    return (int)(width/Sprite::frameCount);
    }

    int Sprite::GetHeight(){return height;}


    void Sprite::SetScaleX (float scale){
        width=(int)((float)GetWidth()*scale*frameCount);
        scaleX=scale;
    }

    void Sprite::SetScaleY (float scale){

        height=(int)((float)GetHeight()*scale);
        scaleY=scale;
    }

    float Sprite::GetScaleX (){
        return scaleX;
    }

    float Sprite::GetScaleY (){
        return scaleY;
    }


    void Sprite::Update(float dt){

    timeElapsed+=dt;

        if (timeElapsed>=frameTime){
            currentFrame+=1;
            if (currentFrame%frameCount==0)
                currentFrame=0;
           SetClip(currentFrame*GetWidth()/scaleX,0,GetWidth()/scaleX,GetHeight()/scaleY);
            timeElapsed=0.0;
        }

    }

    void Sprite::SetFrame (int frame){

       Sprite::currentFrame=frame;
        //seta clip
        SetClip(currentFrame*GetWidth()/scaleX,0,GetWidth()/scaleX,GetHeight()/scaleY);
    }

    void Sprite::SetFrameCount (int frameCount){

    Sprite::frameCount=frameCount;

    }

    void Sprite::SetFrameTime (float frameTime){

    Sprite::frameTime=frameTime;
    }


