#ifndef HEADER_BULLET
#define HEADER_BULLET

#include "Vec2.h"
#include "GameObject.h"
#include "Sprite.h"

class Bullet: public GameObject{


public:

    Bullet(float x, float y,float angle,float speed, float maxDistance, std::string sprite,int frameCount=1,bool Player=false);
    void Render ();
    void Update (float dt);
    bool IsDead();
    void NotifyCollision (GameObject& other);
    bool Is (std::string type);
    bool targetsPlayer;
private:
    Sprite sp;
    Vec2 speed;
    float distanceLeft;
    Vec2 acumulador;
};

#endif

