#ifndef HEADER_PLAYER
#define HEADER_PLAYER
#include "GameObject.h"
#include "Sprite.h"
#include "Vec2.h"
#include "Timer.h"
#include "JoyStick.h"
#include "Objetos.h"


#include "Skill.h"
#include "Projectile.h"
#include "BindSkill.h"
#include "Banana.h"
#include "Boxe.h"
#include "../header/Shield.h"


#include <queue>
#include <vector>
#include "Sound.h"

#define MAX_HP 100
#define SPEED 100

#define SLIDE_DURATION 2

class Player: public GameObject{


public:
    Player (float x,float y, std::string personagem, int nPlayer);
    ~Player ();
    void  Update (float dt);
    void Render ();
    bool  IsDead ();
    void  Shoot ();
    void NotifyCollision (GameObject& other);
    bool Is (std::string type);
    Player* player;
    int nPlayer;
    std::string GetName(){
        return player_name;
    }

    //Skills
    void AddElement(int element);
    void AddSkill(Skill* skill);
    void UseSkill();
    float angle;
private:

    Sprite sp_front;
    Sprite sp_back;
    Sprite sp_left;
    Sprite sp_right;

    int side;
    bool parado=true;
    bool colisao[4];
    Vec2 altspeed;
    Vec2* objeto_colisao;
    std::string player_name;
    Vec2 speed;
    float linearSpeed;
    int hp;

    //Skills
    int elements[3];    //Quais elementos estao ativos
    std::vector<Skill*> skills;
    int activeSkill;    //Qual skill esta ativa baseada nos elementos
    int skillcount;
    Timer cd;
    Timer dash;
    Timer dash_rest;
    Sound* som_dash;
	enum State{NORMAL, BINDED, SLIDE, BACK};
	State state;
	float bindstamp, slidestamp, backstamp;
	bool side_down,side_up,side_right,side_left,key;
	int back_side;
};

#endif
