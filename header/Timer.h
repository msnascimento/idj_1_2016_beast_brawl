
#ifndef HEADER_TIMER
#define HEADER_TIMER


class Timer{

public:
    Timer(){
    time=0.0;
    }
    void Update(float dt){
        time+=dt;
    }
    void Restart(){
        time=0.0;
    }
    float Get(){
    return time;
    }

private:
    float time;


};
#endif
