#ifndef HEADER_JOY
#define HEADER_JOY
#include <iostream>
#define MAX_BUTTONS 17
#define MICRONTEK "Microntek              USB Joystick          "

#define JOY_SELECT 0
#define JOY_START 3
#define JOY_R1 11
#define JOY_R2 9
#define JOY_L1 10
#define JOY_L2 8
#define JOY_TRIANGLE 12
#define JOY_CIRCLE 13
#define JOY_X 14
#define JOY_SQUARE 15
#define JOY_LEFT 7
#define JOY_DOWN 6
#define JOY_RIGHT 5
#define JOY_UP 4

//#define PRINT

class JoyStick{

public:

    JoyStick(SDL_Joystick* j){

        js=j;
        for(int i=0;i<MAX_BUTTONS;i++){
            keyState[i]=false;
            keyUpdate[i]=-1;
        }

        axis[0]=0;
        axis[1]=0;
        nButtons=SDL_JoystickNumButtons(j);
        nome=SDL_JoystickName(j);
    }

    ~JoyStick(){}

    int JoyPress(int button){return keyUpdate[button];}
    bool JoyRelease(int key){
            if(keyState[key])
                return false;
            else return true;
        }
    bool IsJoyDown(int button){
        return keyState[button];
    }

    void SetAxisX(int x){axis[0]=x;}
    void SetAxisY(int y){axis[1]=y;}
    int GetAxisX(){return axis[0];}
    int GetAxisY(){return axis[1];}
    void SetButtonUpdate(int bt, int updateCounter){keyUpdate[bt]=updateCounter;}
    void SetButtonState(int bt, bool state){keyState[bt]=state;}


    int GetNButtons(){return nButtons;}
    std::string GetName(){return nome;}
    SDL_Joystick* GetJoy(){return js;}

    int MapearMicrontek(int j){
        #ifdef PRINT
        std::cout<<j<<std::endl;
        #endif // PRINT
        switch(j){
            case 0:return 12;
            case 1:return 13;
            case 2:return 14;
            case 3:return 15;
            case 4:return 8;    //L2
            case 5:return 9;    //R2
            case 6:return 10;   //L1
            case 7:return 11;   //R1
            case 8:return 0;
            case 9:return 3;
            case 10:return 1;
            case 11:return 2;
            case 12:return 4;
            case 13:return 5;
            case 14:return 6;
            case 15:return 7;
            case 16:return -1;
            default: return -1;
        }
    }

private:
    std::string nome;
    SDL_Joystick* js;
    bool keyState[MAX_BUTTONS];
    int keyUpdate[MAX_BUTTONS];
    int axis[2];
    int id;
    int nButtons;


    /**
    BASE é o controle de ps3. O nũmero de cada botao esta nas imagens nas pasta joystick
    0. select
    1. analogico esquerdo
    2. analogico direito
    3. start
    4. cima
    5. direita
    6. baixo
    7. esquerda
    8. L1
    9. R1
    10. L2
    11. R2
    12. triangulo
    13. bola
    14. x
    15. quadrado
    16. botao meio
    */
};
#endif

