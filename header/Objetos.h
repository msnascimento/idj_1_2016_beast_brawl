
#ifndef H_OBJETOS
#define H_OBJETOS

#include"../header/GameObject.h"
#include "Sprite.h"

class Objetos: public GameObject{


public:
    Objetos();
    Objetos (float x,float y);
    ~Objetos ();
    void  Update (float dt);
    void Render ();
    bool  IsDead ();
    void NotifyCollision (GameObject& other);
    bool Is (std::string type);

    int GetX(){

        return 0;
    }

private:
    Sprite sp;
    int hp;
};

#endif
