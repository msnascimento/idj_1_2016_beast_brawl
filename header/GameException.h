#ifndef HEADER_GEXC
#define HEADER_GEXC

#include <string>

class GameException{
 public:
    GameException(const char* pStr) : pMessage(pStr) {}
    const char* what() const {return pMessage;}

  private:
    const char* pMessage;
};

class SDLException{
 public:
    SDLException(const char* pStr) : pMessage(pStr) {

    }
    const char* what() const {return pMessage;}

  private:
    const char* pMessage;
};

#endif
