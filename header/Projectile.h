#ifndef PROJECTILE_H
#define	PROJECTILE_H

#include <string>
#include "Skill.h"
#include "Sprite.h"
#include "Vec2.h"

#define PROJECTILE_BASE_DMG 10  //dano
#define PROJECTILE_SPEEDMOD 300  //velocidade do tiro
#define PROJECTILE_BASE_CD 1    //Cooldown em segundos
#define PROJECTILE_LAUNCH_SOUND "audio/Sons/WAV/Projetil3.wav"
#define PROJECTILE_SPRITE "img/skills/fuguete.png"    //caminho da imagem do sprite
#define PROJECTILE_CONTACT_SPRITE "img/efeitos/explosao.png"   //caminho do sprite quando o projetil colide
#define PROJECTILE_CONTACT_SOUND "audio/Sons/WAV/Explosion3.wav"       //caminho do som quando o projetil colide

using namespace std;

class Projectile : public Skill{
public:
    Projectile(Vec2 orig, float angle, int source, string spname = PROJECTILE_SPRITE);
    Projectile();   //fabrica de projectiles
    virtual ~Projectile();
    void Update(float dt);
    void Render();
    bool IsDead();
    virtual void NotifyCollision (GameObject& other);
    virtual bool Is (string type);
    int source;
    virtual Skill* NewInstance(Vec2 orig, float angle, int source);
    void SetParameters(float speedmod, float base_cd, int dmg);
    void SetSpeed(float speedmod);
    void SetBaseCD(float base_cd);
    void SetDmg(int dmg);
    int dmg;
protected:
    bool collision;
    Sprite sp;
private:
    float angle;
    float speedmod;
    float BASE_CD;    //Cooldown base
    Vec2 speed;

};

#endif	/* PROJECTILE_H */

